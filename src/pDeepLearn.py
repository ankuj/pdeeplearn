import collections
import copy
import csv  
import itertools
from math import ceil
import re
import os
import sys
import timeit
import lstmImplementation
from collections import defaultdict
from jupyter_client.kernelspecapp import raw_input
"""
this is the code for the main PDeepLearn algorithm
"""
mainlist=[]
parentArgumentDictionary, predicateInformationConstraintDict, predicateBeforeConstraintDict, predicateInformationConstraintDelDict, operatorInformationConstraintDictionary={},{},{},{},{}
actionNumberedDictionary, weightDictionary ={},{'pre':[],'add':[],'del':[], 'weight':[]}
DOMAIN_NAME = None
DATA_FILE = None
PROBLEM_FILE = None
NUMRECORDS = None
ABSPATH = None

def parseIdealModel(idealModelPath):
    with open(idealModelPath, 'r') as idealModelFile:
        idealModelDictionary, actionPredicateDictionary = {},{}
        action = ''
        precondition, effect, negativeEffect = [],[],[]
        for line in idealModelFile:
#             match=re.search(r'\(:action.*:effect.*\)', line, re.DOTALL)
            if ':action ' in line:
                action = re.search(r':action(.*)', line, re.DOTALL).group(1).strip()
            elif ':precondition' in line:
#                 precondition = re.findall(r'\(\w+\s[\?\w+\s?]+\)', line, re.DOTALL)
#following set of changes for the parking domain
                precondition = re.findall(r'\((\w+(-\w+)*)\s[\?\w+\s?]+\)', line, re.DOTALL)
                precondition = [list(i)[0] for i in precondition]
                actionPredicateDictionary['pre'] = precondition
            elif ':effect' in line:
#                 negativeEffect = re.findall(r'not\s\(\w+\s[\?\w+\s?]+\)', line, re.DOTALL)
                negativeEffect = re.findall(r'((not\s\(\w+(-\w+)*)\s[\?\w+\s?]+\))', line, re.DOTALL)
                negativeEffect = [list(i)[1] for i in negativeEffect]                
                print('negativeEffect', negativeEffect)                
                for i in negativeEffect:
                    line = line.replace(i, '')
#                 actionPredicateDictionary['del'] = negativeEffect
#                 effect = re.findall(r'\(\w+\s[\?\w+\s?]+\)', line, re.DOTALL)
#change made for the parking domain
                print('line', line)                
                effect = re.findall(r'\((\w+(-\w+)*)\s[\?\w+\s?]+\)', line, re.DOTALL)
                effect = [list(i)[0] for i in effect]                
                print('effect', effect)                
                actionPredicateDictionary['eff'] = effect+negativeEffect
            if(action and precondition and effect):
                idealModelDictionary[action] = actionPredicateDictionary
                action = ''
                precondition, effect, negativeEffect = [],[],[]
                actionPredicateDictionary = {}
        idealModelFile.close()
        return idealModelDictionary   

# This method applies the rules by ARMS to frequent action pairs
def frequentMiningImplementationARMS(tempPredicateActionMatchDictionary, actionDictionaryForAllModels):
    print('in method frequentMiningImplementation()')
    newTempList=dict()
    with open(str(ABSPATH)+'/generatedFiles/trulesOutput'+DOMAIN_NAME, 'r') as aprioriFile:            
        for row in aprioriFile:
            splitRow=row.split(' ')
            if(len(splitRow)==5 or len(splitRow)==7):
                if(len(splitRow)==5):support=int(splitRow[4].strip())
                if(len(splitRow)==7):support=int(splitRow[4].strip())
                if(support>0):                    
                    key0 = splitRow[0].strip()
                    if(len(splitRow)==5):key1 = splitRow[1].strip()
                    else:key1 = splitRow[2].strip()  
                    newTempList[key0] = []
                    newTempList[key1] = []
                    countOuter, countInner = 0, 0
                    tracker = 0
                    while (countOuter-tracker) < len(tempPredicateActionMatchDictionary[key0]):
                        while (countInner-tracker) < len(tempPredicateActionMatchDictionary[key1]):
                            match=False
                            preListFirstModel=tempPredicateActionMatchDictionary[key0][countOuter-tracker][0][0]
                            addListFirstModel=tempPredicateActionMatchDictionary[key0][countOuter-tracker][0][1]
                            delListFirstModel=list(tempPredicateActionMatchDictionary[key0][countOuter-tracker][1])
                            preListSecondModel=tempPredicateActionMatchDictionary[key1][countInner-tracker][0][0]
                            addListSecondModel=tempPredicateActionMatchDictionary[key1][countInner-tracker][0][1]
                            delListSecondModel=tempPredicateActionMatchDictionary[key1][countInner-tracker][1]
                            constraint1=len(list(set(preListFirstModel).intersection(set(preListSecondModel))))>=ceil(len(preListFirstModel)*0.1)
                            constraint2=len(list(set(addListFirstModel).intersection(set(preListSecondModel))))>=ceil(len(addListFirstModel)*0.1)
                            constraint3=len(list(set(delListFirstModel).intersection(set(addListSecondModel))))>=ceil(len(delListFirstModel)*0.1)
                            if(( constraint1 or  constraint2 or constraint3)):
                                if(tempPredicateActionMatchDictionary[key0][countOuter-tracker] not in newTempList[key0]):
                                    newTempList[key0].append(tempPredicateActionMatchDictionary[key0][countOuter-tracker])
                                    match=True
                                if(tempPredicateActionMatchDictionary[key1][countInner-tracker] not in newTempList[key1]):
                                    newTempList[key1].append(tempPredicateActionMatchDictionary[key1][countInner-tracker])
                                    match=True
                                tempPredicateActionMatchDictionary[key0].remove(tempPredicateActionMatchDictionary[key0][countOuter - tracker])
                                tempPredicateActionMatchDictionary[key1].remove(tempPredicateActionMatchDictionary[key1][countInner- tracker])
                                tracker = tracker +1
                            countInner = countInner +1
                            if(match):
                                break
                        countOuter = countOuter +1
    return newTempList

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el

# This function creates all the PDDL files of the candidate models
def createPDDLFile(templateDictionary, modelReconstructionDictionary, stateDictionary, actionDictionary):
    print('in method createPDDLFile')
    typeLine=''
    alreadyWritten=False
    actualModelDictionary = {}
    global DOMAIN_NAME
    parameterSet = set()
    parameterList = []
    modelReconstructionDictionaryNew, parameterDictionary = {}, {}  
    #cumulate all the types in a list from the stateDictionary
    objectSet = set()
    objectSet = set([j for i in actionDictionary.keys() for j in actionDictionary[i]])
    #convert the numeric dictionary back into the predicate based one
    allModelList = [modelReconstructionDictionary[i] for i in modelReconstructionDictionary.keys()]
    innerList = []
    for key in modelReconstructionDictionary.keys():
        modelReconstructionDictionaryNew[key] = []
        for j in range(len(modelReconstructionDictionary[key])):
            for i in list(flatten(modelReconstructionDictionary[key][j])):
                preList = False
                #find if the element belongs to the preconditions or not, and label it accordingly
                if i in modelReconstructionDictionary[key][j][0][0]:preList = True
                if preList:
                    innerList = innerList + ['(pre:' + (([{**templateDictionary[key][0], **templateDictionary[key][1]}[i]])[0])+')']
                else:
                    innerList = innerList + ([{**templateDictionary[key][0], **templateDictionary[key][1]}[i]])  
            modelReconstructionDictionaryNew[key].append([innerList])
            innerList = []
    allModelList = [modelReconstructionDictionaryNew[key] for key in modelReconstructionDictionaryNew.keys() if not modelReconstructionDictionaryNew[key] == []]
    allModelList = [i for i in itertools.product(*allModelList)]
    #create PDDL domain files which correspond to the candidate models in the allModelList
    flattened_list  = list(flatten(allModelList))
    # start printing the preconditions and effects of the individual actions
    count = 0    
    for i in range(len(allModelList)):
        typeLine = ''
        count = count + 1
        PDDLWriteFile = open(str(ABSPATH)+"/PDDL/allModelFile_"+str(count)+'.pddl', "w")
        PDDLWriteFile.write(';;model: '+str(allModelList[i])+'\n')
        PDDLWriteFile.write('(define (domain '+DOMAIN_NAME+')\n')
        PDDLWriteFile.write('(\t:requirements :strips :typing)\n')
        predicateString = '(:predicates '
        for j in range(len(allModelList[i])):
            parameterSet = set()
            types = '(:types '
            for tt in mainlist[0::2]:
                types = types+' '+tt
            types = types+')'
            for k in allModelList[i][j][0]:
                regExpr = re.findall(r'(\w+(-\w+)*)\[(.*)\]-(\w+)', str(k))
                #add the name of the action to the file
                if allModelList[i][j][0].index(k)==0: typeLine = typeLine + ('\n (:action '+' '+regExpr[0][len(regExpr[0])-1])
                #store all the parameters in a set
                numberOfParameters = len(regExpr[0]) - 3
                for x in regExpr[0][2].split(','):
                    x= (x.replace("'","").replace('"',''))
                    x = re.sub(r'((\w+)\s-)', r'?\1', x)
                    parameterSet.add(x)
            typeLine=typeLine+'\n:parameters ('                    
            for x in parameterSet:
                typeLine = typeLine +' '+ x
            typeLine = typeLine + ')'
            for k in allModelList[i][j][0]:
                precondition = '\n:precondition (and '
                effect  = '\n:effect (and '
                for x in allModelList[i][j][0]:
                    notPredicate = False
                    prePredicate = False
                    if 'not' in str(x):notPredicate = True
                    if 'pre' in str(x):prePredicate = True
                    predicate = re.findall(r'((\w+)(-\w+)*)\[(.*)\]-(\w+)', str(x))
                    variables = [re.search(r'((\w)+(-\w+)*)', i).group(1) for i in predicate[0][3].replace("'","").replace('"','').split(',')]
                    for xx in predicate[0][3].split(','):
                        parameterList.append(xx.replace("'","").replace('"',''))
                    parameterDictionaryValue = '( ' + predicate[0][0] + ' '
                    for jj in parameterList:
                        jj = re.sub(r'((\w+)\s-)', r'?\1', jj)
                        parameterDictionaryValue = parameterDictionaryValue + jj +' '
                    parameterDictionaryValue = parameterDictionaryValue + ' )'
                    parameterDictionary[predicate[0][0]] = parameterDictionaryValue
                    parameterDictionaryValue, parameterList = [], []
                    if prePredicate:
                        precondition = precondition + ' ('+ predicate[0][0]
                        for t in variables:
                            precondition = precondition +' ?'+t
                    else: 
                        if notPredicate: 
                            effect = effect + '(not ('+ predicate[0][0]
                        else: effect = effect + '('+ predicate[0][0]
                        for t in variables:
                            effect = effect +' ?'+t
                    if prePredicate: precondition = precondition + ')'
                    else: effect = effect + ')'
            typeLine = typeLine + precondition + ')'
            typeLine = typeLine + effect + ')))\n'
        for r in parameterDictionary.keys():
            predicateString = predicateString + ' ' + parameterDictionary[r]
        predicateString = predicateString + ')'
        PDDLWriteFile.write(types)
        PDDLWriteFile.write('\n')
        PDDLWriteFile.write(predicateString)
        PDDLWriteFile.write(typeLine)
        PDDLWriteFile.write('\n)')
        PDDLWriteFile.close()
    return allModelList

def extractActionSequenceFromTraces(stateActionString):
    tempStateArgumentList=stateActionString.split(',')
    predicateList=[]
    for i in range(len(tempStateArgumentList)):
        matchList=re.search(r'\(.*\)', tempStateArgumentList[i], re.DOTALL)
        predicateList.append(matchList.group())
    return predicateList    


def preliminaryVerification(j,actionDictionaryForAllModels,tempPredicateActionMatchDictionary,f2Keys):
 #eliminate all models which have a set of predicates less than threshold
#    for key in actionDictionaryForAllModels:
#    print('in method preliminaryVerification()')
    numberOfPredicates=len(dict(list(actionDictionaryForAllModels[f2Keys])[0]).keys())+len(dict(list(actionDictionaryForAllModels[f2Keys])[1]).keys())
    lengthFirstKey=len(j[0][0])+len(j[0][1])
    lengthSecondKey=len(j[1])
#    print('f2keys: ', f2Keys)
#    print('lengthFirstKey ',lengthFirstKey,'lengthSecondKey: ',lengthSecondKey)  
#    print('length before deletion: ', len(tempPredicateActionMatchDictionary[f2Keys]))
    if ((lengthFirstKey+lengthSecondKey)<numberOfPredicates/2):
        tempPredicateActionMatchDictionary[f2Keys].remove(j)
#        print('length after deletion: ', len(tempPredicateActionMatchDictionary[f2Keys]))
#        print('removing model: ',j)
        return tempPredicateActionMatchDictionary
    #also eliminate models which have less than a threshold number of predicates in the pre, add and delete list
    if (lengthFirstKey<=lengthSecondKey):
        tempPredicateActionMatchDictionary[f2Keys].remove(j)
        return tempPredicateActionMatchDictionary
#    if(f2Keys=='move-curb-to-car'):
#        print('model going through: ',j, 'its length: ',lengthFirstKey+lengthSecondKey, 'key is: ', f2Keys)
    return tempPredicateActionMatchDictionary

def recreateOriginalModel(currentModel, tempPredicateActionMatchDictionary,actionPredicateList,actionDictionaryForAllModels,currentActionArguments,f2Keys):
    preListPredicate=[]
    addListPredicate=[]
    delListPredicate=[] 
    print('in method recreateOriginalModel()')
#    tempPredicateActionMatchDictionary=[]  
#    tempPredicateActionMatchDictionary=copy.copy(predicateActionMatchDictionary[actionPredicateList[i][0]]) 
    #figure out the predicate in the add lists and delete lists of the model
    preListPredicatetemp=(re.findall("[0-9]",str(currentModel[0][0]), re.DOTALL))
    for i in range(len(preListPredicatetemp)):
#                                preListPredicate.append(dict(actionDictionaryForAllModels[actionPredicateList[i][0]][0])[int(preListPredicatetemp[i])])
        if(int(preListPredicatetemp[i]) in actionDictionaryForAllModels[f2Keys][0].keys()):
            tempValue=dict(actionDictionaryForAllModels[f2Keys][0])[int(preListPredicatetemp[i])]
            preListPredicate.append(tempValue)
        else:#need to eliminate the model at this stage
            tempPredicateActionMatchDictionary.remove(currentModel)
            return
    addListPredicatetemp=(re.findall("[0-9]",str(currentModel[0][1]), re.DOTALL))
    for i in range(len(addListPredicatetemp)):
#                                addListPredicate.append(dict(actionDictionaryForAllModels[actionPredicateList[i][0]][0])[int(addListPredicatetemp[i])])
#                        addListPredicate=dict(actionDictionaryForAllModels[actionPredicateList[i][0]][0])[int((re.search("[0-9]",str(j[0][1]), re.DOTALL)).group(0))]
        if(int(addListPredicatetemp[i]) in actionDictionaryForAllModels[f2Keys][0].keys()):
            tempValue=dict(actionDictionaryForAllModels[f2Keys][0])[int(addListPredicatetemp[i])]
            addListPredicate.append(tempValue)
        else:
            return
    delListPredicatetemp=re.findall("[0-9]",str(currentModel[1]), re.DOTALL)
    for i in range(len(delListPredicatetemp)):
            if(int(delListPredicatetemp[i]) in actionDictionaryForAllModels[f2Keys][1].keys()):
                tempValue=dict(actionDictionaryForAllModels[f2Keys][1])[int(delListPredicatetemp[i])]
                delListPredicate.append(tempValue)
            else:
                tempPredicateActionMatchDictionary.remove(currentModel)
                return
    completeModel=[''.join(preListPredicate),''.join(addListPredicate),''.join(delListPredicate)]
    preListPredicate=replaceVariablesIntoModel(preListPredicate,addListPredicate, delListPredicate,actionPredicateList,currentActionArguments)
    return preListPredicate

def replaceVariablesIntoModel(preListPredicate,addListPredicate, delListPredicate,actionPredicateList,currentActionArguments):
        print('in method replaceVariablesIntoModel()')
        currentActionDictionary={}
        currentActionDictionaryDel={}
        currentModelDictionaryAdd={}
        currentModelDictionaryDel={}
        currentModelDictionaryPre={}
        for y in range(len(currentActionArguments)):
            splitString=currentActionArguments[y].split('-')
            currentActionDictionary[splitString[0]]=splitString[1]
        y=0
        #at the moment this logic will work in the case of a simple model with single predicates
        variableTypesAddList=(re.findall('\'(\w* - \w*)\'', ''.join(addListPredicate), re.DOTALL))#split the add and delete list into its variables and types
        variableTypesDelList=(re.findall('\'(\w* - \w*)\'', ''.join(delListPredicate), re.DOTALL))
        variableTypesPreList=(re.findall('\'(\w* - \w*)\'', ''.join(preListPredicate), re.DOTALL))
         
        #find where variables are of same name and replace
        #if there are same predicates, their variables need to be different. Eventually consider all possible combinations
        #put the variable names and types and of all 3 lists into a single data structure
         
        for y in range(len(variableTypesAddList)):
            splitString=variableTypesAddList[y].split('-')
            currentModelDictionaryAdd[splitString[0]]=splitString[1]
        for y in range(len(variableTypesDelList)):
            splitString=variableTypesDelList[y].split('-')
            currentModelDictionaryDel[splitString[0]]=splitString[1]
        for y in range(len(variableTypesPreList)):
            splitString=variableTypesPreList[y].split('-')
            currentModelDictionaryPre[splitString[0]]=splitString[1]    
        #find the variables which are the same and can be replaced together
            sameVariableInPreAddLists=set(currentModelDictionaryAdd.keys()).intersection(set(currentModelDictionaryPre.keys()))
            sameVariableInAddDelLists=set(currentModelDictionaryAdd.keys()).intersection(set(currentModelDictionaryDel.keys()))
            #replace variables of action into model (consider simple case at the moment)
            #same condition cannot be in the add list and delete list at the same time
            #first evaluate if the same predicate is present in the add and delete list at the same time
        #combine all dictionaries of add,delete and pre list
        combinedModelDictionary={}
        for d in [currentModelDictionaryAdd,currentModelDictionaryDel, currentModelDictionaryPre]:
            combinedModelDictionary.update(d)
        #if the number of types of variables in the actions and the model are not the same, skip the model 
        if (len(set(combinedModelDictionary.values()))!=len(set(currentActionDictionary.values()))):
            return
        tempCurrentActionDictionary={}
        for keyA in currentActionDictionary:
            if(keyA in tempCurrentActionDictionary.keys()):
                continue
            else:
                for keyC in combinedModelDictionary:
                    if (currentActionDictionary[keyA].strip()==combinedModelDictionary[keyC].strip()):
                        if(keyC in tempCurrentActionDictionary.values()):
                            continue
                        else:
                            tempCurrentActionDictionary[keyA]=keyC
        currentActionDictionary=tempCurrentActionDictionary
        #replace the arguments of the model with the variables of the current action
        for key in currentActionDictionary:    
            preListPredicate=[w.replace(currentActionDictionary[key],key) for w in preListPredicate]
            delListPredicate=[w.replace(currentActionDictionary[key],key) for w in delListPredicate]
            addListPredicate=[w.replace(currentActionDictionary[key],key) for w in addListPredicate]
        #check if the preList satisfies the state condition or not, else exit the model
    #                        preListPredicateSimplified=[re.search(r'(.*\])', preListPredicate, re.DOTALL).group(1)]
        variableTypesPreList=(re.findall('\'(.*)\',?', ''.join(preListPredicate), re.DOTALL))
        variableTypesAddList=(re.findall('\'(.*)\',?', ''.join(addListPredicate), re.DOTALL))
        variableTypesDelList=[]
        for t in delListPredicate:
            variableTypesDelList.append(re.findall('\'(.*)\',?', ''.join(t), re.DOTALL))
        return preListPredicate
def f2ActionApplicability(preListPredicate,initialStateList):
        preListFormatted=[]
#        print('preListPredicate: ',preListPredicate )
        for i in preListPredicate:
            subList=[]
#            arguments=(re.findall(r'\'.*\'',i, re.DOTALL))
            arguments=(re.findall(r'\'\w+-\w+\'', i.strip().replace(' ',''), re.DOTALL))
#            preListPredicate=preListPredicate.replace('[(',',').replace('])',',')
#            print((arguments.group(1)))
#            i="at-curb-num[('car_5- car',)]-move-curb-to-car"
#            arguments=(re.search(r"\(''\)", i.strip(), re.DOTALL))
            predicate=(re.search(r'((\w+-?)+)\[', i.strip(), re.DOTALL))
            subList.append(predicate.group(1).replace(' ',''))
            for j in range(len(arguments)):
                subList.append(arguments[j].replace('\'',''))
#            print(subList)
#            if(len(subList)==0):
#                print('size is zero')
            preListFormatted.append(subList)
            completeMatch=False
#         variableTypesPreList=(re.findall('(\w*- \w*)+', ''.join(preListPredicate), re.DOTALL))
#         completeMatch=False
#         for j in range(len(variableTypesPreList)):
#             completeMatch=False
# #            variableTypesPreList[j]=(''.join(variableTypesPreList).split(','))
#             variableTypesPreList[j]=variableTypesPreList[j].replace('\'','')
# #            preListFormatted.append(re.search(r'(.*)\[', ''.join(preListPredicate), re.DOTALL).group(1).replace(' ',''))
#             preListFormatted.append(re.findall(r'(\w+-)+\[', ''.join(preListPredicate), re.DOTALL))
#         for x in variableTypesPreList:
#             preListFormatted.append(''.join(x).replace(' ',''))
#        if ( (preListFormatted in initialStateList)):
        completeMatch=[i in initialStateList for i in preListFormatted]
#        print('completeMatch: ',completeMatch)
#        if(completeMatch):
#            currentModelScore[j]=0
#            return    
        return completeMatch[0]
def calculateFeatureF2(initialStateList, allModels,actionDictionaryForAllModels, tempPredicateActionMatchDictionary,actionPredicateList,currentActionArguments, f2Keys):
    previousModelPreListPredicate=[]
    currentModel=[]
    completeMatchF2=None
    count=0
#    for currentModel in allModels:
#        tempPredicateActionMatchDictionary=preliminaryVerification(currentModel,actionDictionaryForAllModels,tempPredicateActionMatchDictionary,f2Keys)
    for currentModel in tempPredicateActionMatchDictionary[f2Keys]:
#        print('current model: ', currentModel)
        preListPredicate=recreateOriginalModel(currentModel, tempPredicateActionMatchDictionary,actionPredicateList,actionDictionaryForAllModels,currentActionArguments,f2Keys)
        if(preListPredicate):
            currentModel=list(currentModel)
            if(count==0):
                previousModelPreListPredicate=preListPredicate
            if(preListPredicate==previousModelPreListPredicate and not(completeMatchF2) and count!=0):
                count=count+1
                currentModel.append(0)
                continue
            if(preListPredicate==previousModelPreListPredicate and (completeMatchF2)):
                currentModel.append(1)
                count=count+1
                continue
            completeMatchF2 =f2ActionApplicability(preListPredicate,initialStateList)
            if(completeMatchF2):
                currentModel.append(1)
            else:
                currentModel.append(0)
            previousModelPreListPredicate=preListPredicate
        count=count+1
    return tempPredicateActionMatchDictionary
            #if there is a model for which the condition is untrue, we can assign it at this stage already, to avoid further processing 
#     for j in range(len(variableTypesPreList)):
#         completeMatchF2=False
#         variableTypesPreList[j]=(''.join(variableTypesPreList).split(','))
#         variableTypesPreList[j]=[''.join(i).replace('\'','') for i in variableTypesPreList]
#         preListFormatted=[]
#         preListFormatted.append(re.search(r'(.*)\[', ''.join(preListPredicate), re.DOTALL).group(1).replace(' ',''))
#         for x in variableTypesPreList:
#             preListFormatted.append(''.join(x).replace(' ',''))
#         if (not (preListFormatted in initialStateList)):
#             completeMatchF2=True
#     if(completeMatchF2):
#         return

    previousModelPreListPredicate=[]
def calculateFeatureF1(initialStateList, allModels,actionDictionaryForAllModels, tempPredicateActionMatchDictionary,actionPredicateList,currentActionArguments, f1Keys):
    currentModel=[]
    
    count=0
#    for currentModel in allModels:
#        tempPredicateActionMatchDictionary=preliminaryVerification(currentModel,actionDictionaryForAllModels,tempPredicateActionMatchDictionary,f2Keys)
    for currentModel in tempPredicateActionMatchDictionary[f1Keys]:
#        print('current model: ', currentModel)
        preListPredicate=recreateOriginalModel(currentModel, tempPredicateActionMatchDictionary[f1Keys],actionPredicateList,actionDictionaryForAllModels,currentActionArguments,f1Keys)
        completeMatchF2=False
        if(preListPredicate):
            currentModel=list(currentModel)
            if(count==0):
                previousModelPreListPredicate=preListPredicate
            if(preListPredicate==previousModelPreListPredicate and not(completeMatchF2) and count!=0):
                count=count+1
                currentModel.append(0)
                continue
            if(preListPredicate==previousModelPreListPredicate and (completeMatchF2)):
                currentModel.append(1)
                count=count+1
                continue
            completeMatchF2 =f2ActionApplicability(preListPredicate,initialStateList)
            if(completeMatchF2):
                currentModel.append(int(1))
            else:
                currentModel.append(int(0))
            previousModelPreListPredicate=preListPredicate
            print('currentModel after f1: ', currentModel, 'count: ', count)
        count=count+1
    return tempPredicateActionMatchDictionary
                
def findVariablesSameType(actionDictionary, splitActionToArguments,sameTypeList):
        for i in range(0,len(actionDictionary)):
            if(not(actionDictionary[i]==splitActionToArguments[i])):
                if(not((actionDictionary[i] in sameTypeList) and (splitActionToArguments[i] in sameTypeList))):
                    sameTypeList.append(actionDictionary[i])
                    sameTypeList.append(splitActionToArguments[i])
        return sameTypeList
    
def all_combinations(any_list):
    return itertools.chain.from_iterable(
        itertools.combinations(any_list, i + 1)
        for i in range(len(any_list)-1))

def findAllPossiblePredicateCombinations(predicateActionMatchDictionary, actionDictionary, stateDictionary, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, parentArgumentDictionary, operatorAllCombinationsDictionary, allGeneralizedTraceList):
    print('in method findAllPossiblePredicateCombinations()')
    actionModelDictionary, newString, actionDictionaryForAllModels, numericPredicateDictionary ={},{},{},{}
    global DOMAIN_NAME, KEY0, KEY1
    listModelPreconditions, tempPredicateList, modelsList, tempModelList, placeHolderList, numericPredicateList =[], [], [], [], [], []
    actionModelList=[[]for k in range(len(predicateActionMatchDictionary.keys()))]
    applicablePredicatesAllActions = []
    applicablePredicatesAllActions = list(set([i for j in predicateActionMatchDictionary.values() for i in j]))
    for i in range(len(applicablePredicatesAllActions)):
        applicablePredicatesAllActions.append("not("+''.join(list(applicablePredicatesAllActions)[i])+")")
    applicablePredicatesAllActions.sort()
    for key in predicateActionMatchDictionary:
        tempPredicateList=[]
        tempNotPredicateList=[]
        #represent all the predicates which are applicable to the various actions in the form of a set
        actionPredicateMatchList=predicateActionMatchDictionary[key]
        actionPredicateMatchList.sort()
        for i in range(len(list(actionPredicateMatchList))):
            tempPredicateList.append(list(actionPredicateMatchList)[i]+'-'+key)
            tempNotPredicateList.append("not("+''.join(list(actionPredicateMatchList)[i])+'-'+key+")")
            #create logic to separate the not arguments with all the rest of the arguments, then create lists with all 
            #possible number of arguments. Then divide into 2 to get the add and precondition list
        numberDictionaryForPredicates={}
        numberDictionaryForNotPredicates={}
        actionModelProductList = []
        for i in range(len(tempPredicateList)):
            #group numbers corresponding to predicates which are of the same type
            #the number dictionary could be represented wrt to the actionPredicateMatchList match of the individual action.
            numberDictionaryForPredicates[applicablePredicatesAllActions.index(actionPredicateMatchList[i])] = tempPredicateList[i]
            numberDictionaryForNotPredicates[applicablePredicatesAllActions.index("not("+''.join(list(actionPredicateMatchList)[i])+")")] = tempNotPredicateList[i]
        addDelListAggregator=[]
        addDelListAggregator.append(numberDictionaryForPredicates)
        addDelListAggregator.append(numberDictionaryForNotPredicates)
        actionDictionaryForAllModels[key]=addDelListAggregator#add the model number representations to dictionary
        tempPredicateList=list(numberDictionaryForPredicates.keys())        
        tempPredicateList=list(all_combinations(tempPredicateList))            
        tempNotPredicateList=list(all_combinations(numberDictionaryForNotPredicates.keys()))#all combinations of predicates of single action
        i=0
        tempList=list(itertools.product(tempPredicateList,tempPredicateList))#this logic is questionaable at the moment, could also be replaced by combinations
        nonDuplicateTempList=[]
        #introduce logic to eliminate duplicates in tempPredicateList
        for i in range(len(tempList)):
            preList=tempList[i][0]
            addList=tempList[i][1]
            if(len(set(preList).intersection(set(addList)))==0):
                nonDuplicateTempList.append(tempList[i])
        tempList=nonDuplicateTempList
        ii=list(itertools.product(tempList,tempNotPredicateList))
        nonDuplicateModelList=[]
        #figure out which of the models created have multiple arguments of the same type, so that permutations among them are possible
        for i in ii:
            preList=i[0][0]
            addList=i[0][1]
            delList=i[1]
            nonDuplicateModelList.append(i)
        predicateActionMatchDictionary[key]=nonDuplicateModelList
        actionModelProductList.append(nonDuplicateModelList)
    tempPredicateActionMatchDictionary=predicateActionMatchDictionary.copy()
    nonDuplicateTempList=frequentMiningImplementationARMS(tempPredicateActionMatchDictionary,actionDictionaryForAllModels)        
    allModelList = createPDDLFile(actionDictionaryForAllModels, nonDuplicateTempList, stateDictionary, actionDictionary)
    print('add the model to be tested here')
    enterPressed = raw_input()
    lstmImplementation.mainFunction(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, enterPressed)
    endTime = timeit.default_timer()
    print('running time of program', endTime - startTime )
    print('thats all folks!')

        
def createTRulesGrowthFile(numberStates):
    global NUMRECORDS    
    with open(str(ABSPATH)+"/generatedFiles/labelledTraces.data","r") as csvFile:
        global predicateInformationConstraintDict, predicateInformationConstraintDelDict, DOMAIN_NAME, actionNumberedDictionary
        target = open(str(ABSPATH)+"/generatedFiles/tRuleGrowth"+str(DOMAIN_NAME)+".txt", 'w')
        print('in method createTRulesGrowthFile()')
        count=0
        inreader=csv.reader(csvFile, delimiter=';')
        for row in inreader:
#             if(count > int(NUMRECORDS)):break
            count=count+1
            splitActions=(re.search('\[\s?(.*),?\s?\]', row[0], re.DOTALL)).group(1).split(',')#split each action in to its action name and arguments
#save information regarding the predicate not being deleted by the last action                                    
            tempActionPredicateList = []
            for j in range(0,len(splitActions)):
                splitActionToArguments=splitActions[j].split()
                match=re.search('\((.*)', splitActionToArguments[0], re.DOTALL)
#to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                if(match):
#if we are considering the first action of the trace, then it must be accounted for                    
                    target.write(match.group(1))
                    target.write(" -1 ")
            target.write('-2')                    
            target.write('\n')
    target.close()
    with open(str(ABSPATH)+"/generatedFiles/tRuleGrowth"+str(DOMAIN_NAME)+".txt", "r") as infile:
    #create the negations of each of the predicates in the dictionary
    #If arguments if the same type are in the action, multiple predicates can be added in that case
    #First figure out if many arguments in the action are of the same type
        i=0
        target = open(str(ABSPATH)+"/generatedFiles/newTrule"+str(DOMAIN_NAME)+'.txt', 'w')
        for line in infile:
            for key in actionNumberedDictionary:
                line=str.replace(line, key, str(actionNumberedDictionary[key]))
            target.write(line)
        numberAction=0
    infile.close()
    target.close()
    scanTraces()

# Scan the traces to isolate the predicate specific information, find relevant predicates to each action, and generalize the traces
def scanTraces():
    with open(str(ABSPATH)+"/generatedFiles/labelledTraces.data","r") as csvFile:
        global predicateInformationConstraintDict, predicateInformationConstraintDelDict, NUMRECORDS
        print('in method scanTraces()')
        stateArgumentsList, operatorSequenceList, unitaryOperatorList, actionArgumentsList, sameTypeList, operatorSequenceInnerList, OperatorSequenceOuterList = [],[],[],[],[],[],[]
        valueList=set()
        actionStateMatchList, allGeneralizedTraceList = [],[]
        matchList=[]
        actionStateMatchPair=[]
        tempActionPredicateList=[]
        informationConstraintDictionary, operatorAllCombinationsDictionary, predicateInformationConstraintDictCount, informationConstraintDictionaryInner = defaultdict(dict),{},{},{}
        variableTypePerPredicate=[]
        predicatesWithVariablesList, allTraceGeneralizedList = [],[]
        predicateTypeDictionary,informationConstraintCount,predicateTypeDictionaryPerAction,actionDictionary, StateDictionary, operatorDictionary={},{},{},{},{},{}
        compare = lambda x, y: collections.Counter(x) == collections.Counter(y)
        count=0
        inreader=csv.reader(csvFile, delimiter=';')
        for row in inreader:
            count=count+1
            if(count > NUMRECORDS):break
            print('on row number: ', count)
            splitActions=(re.search('\[\s?(.*),?\s?\]', row[0], re.DOTALL)).group(1).split(',')#split each action in to its action name and arguments
            splitInitialState=(re.search('\[\s?(.*)\]', row[1], re.DOTALL)).group(1).split(',')
            splitFinalState=(re.search('\[\s?(.*)\]', row[2], re.DOTALL)).group(1).split(',')
            splitOperatorStateSequence=(re.search('\[\s?(.*)\]', row[3], re.DOTALL)).group(1).split(',')
            #generalize for state-action sequence to build information constraints
            for j in range(0,len(splitOperatorStateSequence)):
                splitActionToArguments=splitOperatorStateSequence[j].split()
                match=re.search('[operator|state]:\s?\(?(\w+(-\w+)*)\s', splitOperatorStateSequence[j], re.DOTALL)                
                propositionType=['operator' if 'operator' in splitOperatorStateSequence[j] else 'state']
                #to inject an information constraint, the first action must be taken into account. Take into account the first action of every trace
                if(match):
                    #if we are considering the first action of the trace, then it must be accounted for                    
                    if(j==0):
                        firstActionInTrace=match.group(1)
                    for k in range(2,len(splitActionToArguments)):
                        #the search is modified to findall to accommodate multiple '-' that can be found                         
                        matchAction=re.findall('-(\w+)', splitActionToArguments[k], re.DOTALL)
                        #this is changed to matchAction[-1] to accommodate multiple '-' that can be found
                        if(matchAction[-1]):                            
                            actionArgumentsList.append(matchAction[-1])
                    if(count<=30):
                        indexValue=tempActionPredicateList.index(match.group(1)) if (match.group(1)) in tempActionPredicateList else -1
                        if(indexValue!=-1):
                            #find match between actionDictionary[match.group()] and splitActionToArguments
                            tempMatchList=findVariablesSameType(operatorDictionary[match.group(1)],actionArgumentsList,sameTypeList)
                        else:  
                            tempActionPredicateList.append(match.group(1)) 
                    operatorDictionary[match.group(1)]=actionArgumentsList#add actions to dictionary
                    if(count<=30 and propositionType[0]=='operator'):                    
                        if match.group(1) not in operatorAllCombinationsDictionary.keys(): 
                            operatorAllCombinationsDictionary[match.group(1)] =  [actionArgumentsList]
                        else:
                            operatorAllCombinationsDictionary[match.group(1)] = operatorAllCombinationsDictionary[match.group(1)] + [actionArgumentsList]
                    unitaryOperatorList.append(propositionType[0])
                    unitaryOperatorList.append(match.group(1))
                    unitaryOperatorList.append(actionArgumentsList)
                    #sequentially add the operators/predicates with their argument types as they appear in the list. The order is important to impose the information constraint
                    operatorSequenceList.append(unitaryOperatorList)
                actionArgumentsList=[]
                unitaryOperatorList=[]
            for key in operatorAllCombinationsDictionary.keys():
                b_set = set(tuple(x) for x in operatorAllCombinationsDictionary[key])
                operatorAllCombinationsDictionary[key] = [ list(x) for x in b_set ]
            tempOperatorSequenceList = copy.copy(operatorSequenceList)
            i = 0
            tempActionPredicateList = []
            allGeneralizedTraceList.append(tempOperatorSequenceList)
            for i in range(len(operatorSequenceList)):
                if(count > 50):break
                tracker = 0
                belongs = False
                operatorSequenceOuterList, operatorSequenceInnerList = [],[]
                operatorIndices = [j for j, x in enumerate(operatorSequenceList) if x[0] == 'operator']
                if operatorSequenceList[i][0]=='state':
#add the constraint information to the state dictionary                                    
                    if (operatorSequenceList[i][1] not in StateDictionary.keys()):
                        StateDictionary[operatorSequenceList[i][1]] = [operatorSequenceList[i][2]]
                    else:
                        StateDictionary[operatorSequenceList[i][1]] = StateDictionary[operatorSequenceList[i][1]]+[operatorSequenceList[i][2]]
            tempActionPredicateList = []
            for j in range(0,len(splitActions)):
                splitActionToArguments=splitActions[j].split()
                match=re.search('\((.*)', splitActionToArguments[0], re.DOTALL)
                if(match):
                    #if we are considering the first action of the trace, then it must be accounted for                    
                    if(j==0):
                        firstActionInTrace=match.group(1)
                    #here is a good place to add code for finding predicates of the same type
                    #if match.group() already exists in the list which consists of all the actions
                    for k in range(1,len(splitActionToArguments)):
                        matchAction=re.findall('-(\w+)', splitActionToArguments[k], re.DOTALL)
                        #code being added as matchAction isnt always found
                        if not matchAction[-1]:continue
                        actionArgumentsList.append(matchAction[-1])
                    if(count<=10):
                        indexValue=tempActionPredicateList.index(match.group(1)) if (match.group(1)) in tempActionPredicateList else -1
                        if(indexValue!=-1):
                       #find match between actionDictionary[match.group()] and splitActionToArguments
                            tempMatchList=findVariablesSameType(actionDictionary[match.group(1)],actionArgumentsList,sameTypeList)
                        else:  
                            tempActionPredicateList.append(match.group(1)) 
                    
                    actionDictionary[match.group(1)]=actionArgumentsList#add actions to dictionary
                actionArgumentsList=[]
            for j in range(0,len(splitInitialState)):
                signatureMatch = False
                splitInitialStateToArguments=splitInitialState[j].split()
                match=re.search('\((\w+(-\w+)*)', splitInitialStateToArguments[0], re.DOTALL)
                for k in range(1,len(splitInitialStateToArguments)):
                    matchPred=False
                    matchPredInner = False
                    matchState=re.findall('-(\w+)', splitInitialStateToArguments[k], re.DOTALL)
                    #continue statement is added here to ensure the code does not block in case a match isnt found
                    if not matchState[-1]:continue
                    stateArgumentsList.append(matchState[-1])
                #the predicate and arguments need to be matched against the action to see if the predicates are applicable to the actions or not                    
                for tempSingleActionList in operatorAllCombinationsDictionary[firstActionInTrace]:                
                    indices = [l for l, m in enumerate(stateArgumentsList) for tt in tempSingleActionList if m==tt]
                    if(len(list(set(indices)))==len(stateArgumentsList)):
                        signatureMatch = True
                if(match):
                    if (match.group(1) not in StateDictionary.keys()):
                        StateDictionary[match.group(1)]=[stateArgumentsList]
                    else:
                        for k in StateDictionary[match.group(1)]:
                            if (compare(k,stateArgumentsList)):
                                matchPred=True
                                break
                        if not(matchPred):
                            StateDictionary[match.group(1)].append(stateArgumentsList)
                    #if predicate is applicable to the action then the predicate must be added to the informationConstraintDictionary
                    if(signatureMatch):
                        if (match.group(1) not in informationConstraintDictionary[firstActionInTrace].keys()):
                            informationConstraintDictionary[firstActionInTrace][match.group(1)]=[stateArgumentsList]
                        else:
                            for k in informationConstraintDictionary[firstActionInTrace][match.group(1)]:
                                if (compare(k,stateArgumentsList)):
                                    matchPredInner=True
                                    break
                            if not(matchPredInner):
                                informationConstraintDictionary[firstActionInTrace][match.group(1)].append(stateArgumentsList)
                stateArgumentsList=[]
            for j in range(0,len(splitFinalState)):
                matchPred=False
                splitFinalStateToArguments=splitFinalState[j].split()
                match=re.search('\((.*)', splitFinalStateToArguments[0], re.DOTALL)
                for k in range(1,len(splitFinalStateToArguments)):
                    matchState=re.findall('-(\w+)', splitFinalStateToArguments[k], re.DOTALL)
                    if not matchState[-1]: continue
                    stateArgumentsList.append(matchState[-1])
                if(match):
                    if (match.group(1) not in StateDictionary.keys()):
                        StateDictionary[match.group(1)]=[stateArgumentsList]
                    else:
                        for k in StateDictionary[match.group(1)]:
                            if (compare(k,stateArgumentsList)):
                                matchPred=True
                                break
                        if not(matchPred):
                            StateDictionary[match.group(1)].append(stateArgumentsList)
                stateArgumentsList=[]
            #whatever predicates we find in the predicateInformationConstraintDict should be added to the stateDictionary 
            for x in predicateInformationConstraintDict.keys():
                match =  re.search(r'(\w+(-\w+)*)\[(.*)\]',x)              
                predicateName = match.group(1)
                arguments = match.group(3)
                if predicateName not in StateDictionary.keys():
                    StateDictionary[predicateName] = arguments
            if not firstActionInTrace in informationConstraintCount.keys():
                informationConstraintCount[firstActionInTrace]=1
            else:
                informationConstraintCount[firstActionInTrace]=informationConstraintCount[firstActionInTrace]+1
        #eliminate duplicates from the informationConstratintList and the StateDictionary
        for key in StateDictionary.keys():
            StateDictionary[key] = [i for i in map(list, set(map(tuple, StateDictionary[key])))]
        #figure out if all the elements of one list can be part of another list or not
        predicatesForSameAction=[]
        tempIndiceOuterList = []
        predicateInMatchList=[]
        global parentArgumentDictionary
        predicateActionMatchDictionary={}
        for i in tempMatchList:
            tempIndiceInnerList = []
            listAddition = False
            indices = [j for j, x in enumerate(tempMatchList) if x == i]
            indices = [j+1 if j%2==0 else j-1 for j in indices]
            tempIndiceInnerList.append(i)
            for t in indices:
                tempIndiceInnerList.append(tempMatchList[t])
            #figure out if this inner list already exists in the outer list
            for x in tempIndiceOuterList:
                x.sort()
                tempIndiceInnerList.sort()
                if (set(x).issubset(set(tempIndiceInnerList))):
                    tempIndiceOuterList.remove(x)
                    listAddition = True
                    break
                elif(set(tempIndiceInnerList).issubset(set(x))):
                    listAddition = False
                    break
                else:
                  listAddition = True
            if(listAddition or tempIndiceOuterList == []): 
                tempIndiceOuterList.append(tempIndiceInnerList) 
                listAddition = False
        #change the parent argument dictionary to integrate the changes made above
        for i in tempIndiceOuterList:
            key = ''
            for j in i:
                key = key+ j[0] 
            parentArgumentDictionary[key] = i        
            
        for key in actionDictionary:
            singleActionList=actionDictionary[key]
            for stateKey in StateDictionary:
                tempSingleActionList=singleActionList[:]
                predicateTypeList=[]
                predicatesForSameActionTemp=[]
                matchedVariables=[]
                singleStateList=StateDictionary[stateKey]
                instantiatedVariablesPerAction=[]
                tempSingleStateList=copy.deepcopy(singleStateList)
                variableTypePerPredicate=[]
                for k in tempSingleStateList:
                    matchedVariablesInner, allMatchedVariables=[],[]
                    tempTempSingleActionList=copy.deepcopy(tempSingleActionList)
                    for r in k:
                        intersectionElements=set(tempTempSingleActionList).intersection(set([r]))
                        if(len(intersectionElements)!=0):
                            matchedVariablesInner.append(list(intersectionElements)[0])
                            tempTempSingleActionList.remove(list(intersectionElements)[0])
                    #make 3 cases for no match, partial and complete match
                    #case when subset of the variables is repeated and multiple matches can be created needs to be added
                    t=copy.copy(k)
                    if(matchedVariablesInner!=[] and len(matchedVariablesInner)==len(k)):
                        allMatchedVariables.append(matchedVariablesInner)
                    #find the max frequency of all the variables in all the combinations of the actions
                    objectFrequencyDictionary, elementFrequencyDictionary = {}, {}
                    for i in operatorAllCombinationsDictionary[key]:
                        elementFrequencyDictionary = collections.Counter(i)
                        if (objectFrequencyDictionary =={}): objectFrequencyDictionary = elementFrequencyDictionary
                        else:
                            for j in elementFrequencyDictionary.keys():
                                if j in objectFrequencyDictionary.keys():
                                    objectFrequencyDictionary[j] = max(elementFrequencyDictionary[j], objectFrequencyDictionary[j])
                    changedArgumentList = []
                    if((len(matchedVariablesInner)==0 or len(k)>len(matchedVariablesInner)) and changedArgumentList==[]):#case when no match is found
                        continue
                    for i in changedArgumentList:
                        if i!=[] and i not in allMatchedVariables:
                            allMatchedVariables.append(i)
                    predicateFrequencyList=dict()
                    instantiatedVariablesPerAction=[]
                    t=i
                    c=0
                    for matchedVariables in allMatchedVariables:
                        tempTempSingleActionList=tempSingleActionList[:]
                        for tt in matchedVariables:
                            predicateFrequencyList[tt]=objectFrequencyDictionary[tt]
                        tempTempSingleActionList=tempSingleActionList[:]
                        for j in range(max(predicateFrequencyList.values())):
                            tempInstantiatedVariablesPerAction=[]
                            tempVariableTypePerPredicate=[]
                            if(len(set(matchedVariables))!=len(matchedVariables)):
                                t=0
                                for i in list(matchedVariables):
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+list(i)[2]+str(t)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                                    t=t+1
                                instantiatedVariablesPerAction.append(tempInstantiatedVariablesPerAction)
                                indices = [l for l, m in enumerate(variableTypePerPredicate) for i in tempVariableTypePerPredicate if m==i]
                                if( not indices):                            
                                    variableTypePerPredicate.append(tempVariableTypePerPredicate)
                                    variableTypePerPredicate.append(tempInstantiatedVariablesPerAction)   
                                break
                            for i in list((matchedVariables)):
                                if(j>0 and j==predicateFrequencyList[i]):
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+list(i)[2]+str(j-1)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                                else:
                                    tempInstantiatedVariablesPerAction.append(list(i)[0]+list(i)[1]+list(i)[2]+str(j)+" - "+i)
                                    tempVariableTypePerPredicate.append(i)
                            instantiatedVariablesPerAction.append(tempInstantiatedVariablesPerAction)
                            #add it alternatively in order to mantain order in the case of the constraint creation step which comes after 
                            variableTypePerPredicate.append(tempVariableTypePerPredicate)
                            variableTypePerPredicate.append(tempInstantiatedVariablesPerAction)
   
                    for j in instantiatedVariablesPerAction:
                        predicatesForSameAction.append(str(str(stateKey)+str(j)))
                    predicateTypeDictionaryPerAction[stateKey]=variableTypePerPredicate
                    #add dictionary to save the names and variable types of the actions                            
                    predicatesWithVariablesList.append(str(str(stateKey)+str(variableTypePerPredicate)))
                    predicatesForSameActionTemp.append(str(str(stateKey)+str(instantiatedVariablesPerAction)))
                    if(len(predicateTypeList)!=0):
                        predicatesWithVariablesList.append(predicateTypeList)
                    tempSingleActionList=singleActionList[:]       
                    predicateActionMatchDictionary[key]=(predicatesForSameAction)
                    predicateTypeDictionary[key]=predicateTypeDictionaryPerAction
            predicatesForSameAction=[]
            predicatesWithVariablesList=[]
            predicateTypeDictionaryPerAction={}
        findAllPossiblePredicateCombinations(predicateActionMatchDictionary, actionDictionary, StateDictionary, predicateTypeDictionary, informationConstraintDictionary, informationConstraintCount, parentArgumentDictionary, operatorAllCombinationsDictionary, allGeneralizedTraceList)

def changeTypeFindMatchAction(tempMatchList, stateList, singleActionList, operatorAllCombinationsDictionary):
#change the type signature of the actions and try find matches with the predicates    
    tempSingleActionList=copy.deepcopy(singleActionList)
#this sorting is done as a bug fix as the number of constraints produced in the end seem to vary in number    
    operatorAllCombinationsDictionary.sort()
    validIndices = []
#match each variable of stateList and change one by one and match with singleActionList 
    for i in singleActionList:
        indexes = [l for l, m in enumerate(tempMatchList) if m==i]
        if indexes!=[]:
            for j in indexes:
        #figure out if the index is even or odd, and put in the replacement index accordingly 
                if (j%2)==0:
                    j = j+1
                else:
                    j = j-1
                tempSingleActionList=copy.deepcopy(singleActionList)
                tempSingleActionList[singleActionList.index(i)] = tempMatchList[j]
#check if this action tempSingleActionList actually appears in the traces or not. If it doesn't it isn't a valid option
                if tempSingleActionList in operatorAllCombinationsDictionary:                
                    indices = [l for l, m in enumerate(stateList) for i in tempSingleActionList if m==i]
    #                 if(len(indices)==len(stateList) and tempMatchList[j] in [tempSingleActionList[x] for x in indices]):
                    if(len(list(set(indices)))==len(stateList) and tempMatchList[j] in [stateList[x] for x in indices]):
                        validIndices.append(stateList) 
    return validIndices


# replace the objects in the traces with their types
def matchAndReplacePredicates(mainlist):
    global NUMRECORDS
    with open(str(ABSPATH)+'/generatedFiles/labelledTraces.data','w') as csvFile, open (str(ABSPATH)+DATA_FILE,"r") as csvReaderFile:        
        print('in method matchAndReplacePredicates')
        count=0
        numberStates=0
        for row in csvReaderFile:
            splitString = row.split(';')
            for columnString in splitString:
                if(columnString.strip()!=''):
                    for j in range(len(mainlist)):
                        if(j%2!=0):
                            for k in range(len(mainlist[j])):
                                columnString=''.join(columnString)
                                mainlist[j][k]=''.join(mainlist[j][k])
                                mainlist[j-1]=''.join(mainlist[j-1])
                                columnString = columnString.replace(mainlist[j][k]+' ',mainlist[j][k]+'-'+mainlist[j-1]+' ').replace(mainlist[j][k]+')',mainlist[j][k]+'-'+mainlist[j-1]+')')
                    csvFile.write(columnString+';')
            csvFile.write('\n')
            count = count + 1
    csvFile.close()
    csvReaderFile.close()
    createTRulesGrowthFile(numberStates)  
    
    
# read the objects from the problem file    
def readPredicates():
    with open(str(ABSPATH) + PROBLEM_FILE,"r") as infile, open(str(ABSPATH) +"/generatedFiles/predicates.csv","w") as csvfile:                        
            print('in method readPredicates()')
            newline = infile.read();
            match=re.search(r'\(:objects\n.*?\)', newline, re.DOTALL)
            csvfile.write(match.group(0).lower())
    infile.close();
    csvfile.close();
    separateObjects();
    

# separate objects from their types    
def separateObjects():
    with open(str(ABSPATH)+"/generatedFiles/predicates.csv", "r") as csvfile, open(str(ABSPATH)+"/generatedFiles/objects.csv", "w") as objectsFile:
        print('in method separateObjects')
        global mainlist
        inreader = csv.reader(open(str(ABSPATH)+"/generatedFiles/predicates.csv","r"))
        for row in inreader:
            rowString=','.join(row)
            match=re.search(r'(.*)- (\w*)', rowString)
            if match:
                sublist=[]
                arguments=match.group(1)
                types=match.group(2)
                argumentsAsString=arguments.split()
                mainlist.append(types)
                present=False
                for j in argumentsAsString:
                    if(len(sublist)==0):
                        sublist.append(j)
                    else:
                        for k in (sublist):
                            if (k in j):
                                present=True
                                break
                        if not(present):
                            sublist.append(j)
                mainlist.append(sublist)
    csvfile.close()
    objectsFile.close()
    matchAndReplacePredicates(mainlist)   
    
def main(): 
    global DATA_FILE, PROBLEM_FILE, DOMAIN_NAME, NUMRECORDS, ABSPATH
    ABSPATH = os.getcwd()
    DOMAIN_NAME = sys.argv[1]   
    DATA_FILE = sys.argv[2]
    NUMRECORDS = int(sys.argv[3])
    PROBLEM_FILE = sys.argv[4]
    readPredicates();
    
if __name__ == "__main__":
    startTime = timeit.default_timer()
    main()
    
        

    
    
    
