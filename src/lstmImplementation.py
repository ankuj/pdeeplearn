import tflearn
import numpy
import collections
import tensorflow as tf
import copy

def mainFunction(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, numericPredicateDictionary):    
    #code to add padded data
    train_input, train_output, test_input, maxTraceLength, vectorLen = operatorSpecificEncodingScheme(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, numericPredicateDictionary)
    train_input, test_input = train_input[:int(len(train_input)*0.8)], test_input[int(len(test_input)*0.8):]
    train_output, test_output = train_output[:int(len(train_output)*0.8)], train_output[int(len(train_output)*0.8):] #everything beyond 10,000
    # Network building
    net = tflearn.input_data([None, 1, vectorLen])
    train_input, test_input = numpy.reshape(train_input, (-1, 1, vectorLen)), numpy.reshape(test_input, (-1, 1, vectorLen))
    net = tflearn.lstm(net, 128, dropout=0.8)
    #always change the second argument in the statement below to account for the change in domain
    net = tflearn.fully_connected(net, len(list(actionDictionaryForAllModels.keys())), activation='softmax')
    net = tflearn.regression(net, optimizer='adam', learning_rate=0.001,
                             loss='categorical_crossentropy')
    # Training
    model = tflearn.DNN(net, tensorboard_verbose=0)
    model.fit(train_input, train_output, validation_set=(test_input, test_output), show_metric=True,
              batch_size=maxTraceLength)

def operatorSpecificEncodingScheme(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, currentModel):
    actionAppearanceOrderDictionary, predicatesPerActionDictionaryCount, finalVectorDictionary = {}, {}, {}
    finalVector, finalVectorTrace, finalTestVector, trainingOutputVector, testOutputVector, finalVectorCorpus = [], [], [], [], [], []
    operatorNum, finalOutputVectorTrace, finalOutputVectorCorpus = [],[],[]
    finalTestVectorTrace, finalTestVectorCorpus = [],[]
    currentStatePredicates, nextStatePredicates = set(), set()
    #create a dictionary to match the position in the vector with the number labeling the predicate in the actionDictionaryForAllModels
    positionPredicateNumberMatchDict = dict()
    #find the maximum number of operators in all of the traces combined
    for y in range(len(allGeneralizedTraceList)):
        operatorIndices = [j for j, x in enumerate(allGeneralizedTraceList[y]) if x[0] == 'operator']
        operatorNum.append(len(operatorIndices))
    maxOperatorLen = max(operatorNum)
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())+len(actionDictionaryForAllModels[key][1].keys())
    inputVectorLen = sum(predicatesPerActionDictionaryCount.values()) + len(list(actionDictionaryForAllModels.keys()))
    for y in range(inputVectorLen):
        finalVector.append(0)  
        finalTestVector.append(0)
    allPredicateList = [i for i in flatten([list(actionDictionaryForAllModels[key][0].values()) + list(actionDictionaryForAllModels[key][1].values()) for key in actionDictionaryForAllModels.keys()])]
    currentModel = [i for i in flatten((currentModel))]
    count = 0
    for y in allPredicateList:
        finalVectorDictionary[len(actionAppearanceOrderDictionary.keys()) + count] = y
        count = count + 1
    for i in range(len(allGeneralizedTraceList)):
        print('at record number: ', i)
        operatorIndices = [q for q, x in enumerate(allGeneralizedTraceList[i]) if x[0] == 'operator']
        predicateIndices = [qq for qq, x in enumerate(allGeneralizedTraceList[i]) if x[0] == 'state']
        for j in range(1,len(allGeneralizedTraceList[i])):
            if 'operator' in allGeneralizedTraceList[i][j]:
                for y in range((inputVectorLen)):
                    finalVector[y] = 0  
                currentAction = allGeneralizedTraceList[i][j][1]
                #continue as long as the last action is not reached
                if (operatorIndices.index(j))!=(len(operatorIndices)-1):
                    nextAction = allGeneralizedTraceList[i][operatorIndices[operatorIndices.index(j)+1]][1]
                    nextActionIndex = operatorIndices[operatorIndices.index(j)+1]
                previousActionIndex = operatorIndices[operatorIndices.index(j)-1]
                #find the indices between the current operator and the previous one
                currentStateIndices = [k for k in predicateIndices if k<j and k>previousActionIndex]
                nextStateIndices = [k for k in predicateIndices if k>j and k<nextActionIndex]                
                for x in currentStateIndices:
                    #Create a set to to eleminate the number of duplicates in the predicates of the state 
                    currentStatePredicates.add(allGeneralizedTraceList[i][x][1])
                for x in nextStateIndices:
                    #Create a set to to eleminate the number of duplicates in the predicates of the state 
                    nextStatePredicates.add(allGeneralizedTraceList[i][x][1])
                #find the difference between the current and next state sets to figure out the predicates that are added
                currentNextStateDifference = nextStatePredicates.difference(currentStatePredicates) 
                nextState = allGeneralizedTraceList[i][j+2]
                #encode the current action into the vector form
                for k in actionAppearanceOrderDictionary.keys():
                    if(k==currentAction):
#                         actionVectorRepresentationDict[k]=1
                        finalVector[actionAppearanceOrderDictionary[k]] = 1
                        finalTestVector[actionAppearanceOrderDictionary[k]] = 1
                    else:   
#                         actionVectorRepresentationDict[k]=0
                        finalVector[actionAppearanceOrderDictionary[k]] = 0
                        finalTestVector[actionAppearanceOrderDictionary[k]] = 0
                #encode the next action into the vector form
                for k in actionAppearanceOrderDictionary.keys():
                    if(k==nextAction):
                        trainingOutputVector.insert(actionAppearanceOrderDictionary[k], 1)
                    else:   
                        trainingOutputVector.insert(actionAppearanceOrderDictionary[k], 0)
                #Find out which predicates from the current state are relevant to the action being applied.
                for t in currentStatePredicates:
                    for tt in actionDictionaryForAllModels[currentAction][0].values():
                        if t in tt:
                            predicateIndex = [k for (k, v) in finalVectorDictionary.items() if v == tt]
                            finalVector[predicateIndex[0]] = 1
                            break                            
                #Find out which predicates from the state difference between the current and next state are relevant to the action being applied.
                for t in currentNextStateDifference:
                    for tt in actionDictionaryForAllModels[currentAction][0].values():
                        if t in tt:
                            predicateIndex = [k for (k, v) in finalVectorDictionary.items() if v == tt]
                            finalVector[predicateIndex[0]] = 1 
                            break                           
                #switch the predicates corresponding to the current action for the current model being tested as 1
                currentPredicatesTestModel = [kk for kk in currentModel if currentAction in kk]
                for yy in currentPredicatesTestModel:
                    if 'pre' in yy: yy = yy.replace('(pre:','').replace(')','')
                    #find index of the current predicate in the vector
                    predicateIndexTest = [k for (k, v) in finalVectorDictionary.items() if v == yy]
                    if predicateIndexTest: finalTestVector[predicateIndexTest[0]] = 1
                finalVectorTrace.append(numpy.array(finalVector))
                finalOutputVectorTrace.append(numpy.array(trainingOutputVector))
                finalTestVectorTrace.append(numpy.array(finalTestVector))                
                trainingOutputVector = []
        finalVectorCorpus.append(finalVectorTrace)
        finalTestVectorCorpus.append(finalTestVectorTrace)
        finalOutputVectorCorpus.append(finalOutputVectorTrace)
        #pad the input and output vector corpus to the certain max length of the entire batch
        finalVectorTrace = []
        finalTestVectorTrace = []
        finalOutputVectorTrace = [] 
    finalTestOutputVectorCorpus = copy.copy(finalOutputVectorCorpus)  
    finalVectorCorpus, finalOutputVectorCorpus = padTraceToTemp(finalVectorCorpus, maxOperatorLen, inputVectorLen, finalOutputVectorCorpus, len(list(actionAppearanceOrderDictionary.keys())))
    finalTestVectorCorpus, finalTestOutputVectorCorpus = padTraceToTemp(finalTestVectorCorpus, maxOperatorLen, inputVectorLen, finalTestOutputVectorCorpus, len(list(actionAppearanceOrderDictionary.keys())))
    return finalVectorCorpus, finalOutputVectorCorpus, finalTestVectorCorpus, maxOperatorLen, inputVectorLen

def padTraceToTemp(entireTraceAsVectorList,maxLen,vectorLen,entireFileOutputVectorList, outputVectorLen):
    tempEntireFileOutputVectorList=[]
    count=0
    for i in entireTraceAsVectorList:
        padNumber=maxLen-len(i)
        for j in range(padNumber):
            tempList=[]
            tempOutputVectorList=[]
            for k in range((vectorLen)):
                tempList.append(0)
            for x in range(outputVectorLen):
                tempOutputVectorList.append(0)
            i.append(numpy.array(tempList))
            entireFileOutputVectorList[count].append(numpy.array(tempOutputVectorList))
        count=count+1
    flattenedList = [numpy.array(val) for sublist in entireTraceAsVectorList for val in sublist]
    flattenedOutputVectorList = [val for sublist in entireFileOutputVectorList for val in sublist]
    return  flattenedList,flattenedOutputVectorList

def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el
