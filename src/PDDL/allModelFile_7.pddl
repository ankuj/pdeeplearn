;;model: ([["(pre:at-robby['rob0 - robot', 'roo1 - room']-move)", "at-robby['rob0 - robot', 'roo0 - room']-move", "not(at-robby['rob0 - robot', 'roo1 - room']-move)"]], [["(pre:at-robby['rob0 - robot', 'roo0 - room']-pick)", "at['bal0 - ball', 'roo0 - room']-pick", "not(free['rob0 - robot', 'gri0 - gripper']-pick)"]], [["(pre:at-robby['rob0 - robot', 'roo0 - room']-drop)", "at['bal0 - ball', 'roo0 - room']-drop", "not(carry['rob0 - robot', 'bal0 - ball', 'gri0 - gripper']-drop)"]])
(define (domain gripper)
(	:requirements :strips :typing)
(:types  robot gripper room ball)
(:predicates  ( at-robby ?rob0 - robot  ?roo0 - room  ) ( carry ?rob0 - robot  ?bal0 - ball  ?gri0 - gripper  ) ( at ?bal0 - ball  ?roo0 - room  ) ( free ?rob0 - robot  ?gri0 - gripper  ))
 (:action  move
:parameters (  ?roo1 - room ?rob0 - robot  ?roo0 - room)
:precondition (and  (at-robby ?rob0 ?roo1))
:effect (and (at-robby ?rob0 ?roo0)(not (at-robby ?rob0 ?roo1))))

 (:action  pick
:parameters ( ?bal0 - ball ?rob0 - robot  ?roo0 - room  ?gri0 - gripper)
:precondition (and  (at-robby ?rob0 ?roo0))
:effect (and (at ?bal0 ?roo0)(not (free ?rob0 ?gri0))))

 (:action  drop
:parameters ( ?bal0 - ball ?rob0 - robot  ?roo0 - room  ?gri0 - gripper  ?bal0 - ball)
:precondition (and  (at-robby ?rob0 ?roo0))
:effect (and (at ?bal0 ?roo0)(not (carry ?rob0 ?bal0 ?gri0))))

)