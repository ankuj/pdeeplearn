import collections
import os
import numpy as np
import tensorflow as tf
import json
import pickle
import copy
import re
import csv
from collections import defaultdict

MAX_VECTOR_LENGTH=26

#function which converts an action into a vector
def  trainAllPredicateTestModel():
    traceList,entireFileOutputVectorList, entireFile, predicatesPerAction, preconditionList, addconditionList, postconditionList, entireTraceAsTestVectorList, entireFileAsTestVectorList=[],[],[],[],[],[],[],[],[]
    predicatesPerActionDictionaryCount, predicatesPerActionTestModelDict={},{}
    actionDictionaryForAllModels={}
    actionAppearanceOrderDictionary={}
    predicatesPerActionDict={}
    actionVectorRepresentationDict={}
    predicateVectorRepresentationDict={}
    count=0
    entireFileAsVectorList, trainingOutputVector, entireActionAsVectorList, entireFileAsVector, traceLengthArray, entireTraceAsVectorList, finalVector, postconditionList, finalTestVector=[],[],[],[],[],[],[],[],[]
    #open file which contains cartesian product of all the models and pick them up one by one
#     with open('src/parking/allPredicateNumberDictionary', 'r') as file:
#     with open('src/depots/dictionaries/allPredicateNumberDictionarysatellite28jan.txt', 'r') as file:
    with open('src/depots/dictionaries/allPredicateNumberDictionarymprime.txt', 'r') as file:        
#     with open('src/depots/dictionaries/allPredicateNumberDictionarygripper.txt', 'r') as file:
#     with open('src/depots/dictionaries/allPredicateNumberDictionarydepots.txt', 'r') as file:                
        actionDictionaryForAllModels=json.load(file)
    file.close()
#command to find the current working directory    
#    print(os.getcwd())
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())+len(actionDictionaryForAllModels[key][1].keys())
    inputVectorLen=sum(predicatesPerActionDictionaryCount.values())       
    allModelFile=open("src/learning/modelGeneration/generated/satellite/allModelFile_satellite","rb")
    itemList=pickle.load(allModelFile)  
    for currentModel in itemList:
#hard code ideal model at this stage as we want to train with the ideal model
        currentModel=((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, 10)), (9, )), (((40, 44, 50), (42,)), (45, )))
#hard code the model to be tested as we want to see how accurately the new model fares in the testing
#parking domain
#         testModel= ((((0,), (2,)), (1,)), (((16,), (18,)), (17,)), (((32,), (34,)), (33,)), (((48,), (50,)), (49,)))
#         testModel= ((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, )), (9, )), (((40, 44, 50), (42,)), (45, )))
#satellite domain
#         testModel= ((((0, ), (1, )), (2, )), (((34, 38), (32, )), (35, )), (((36, 18), (20, )), (17, 19)), (((40, 41, 22, 28), (24,)), ()), (((6, 8, 12, 43, 44), (14, )), ()))
#mprime model        
        testModel= ((((0, 4), (2, 6)), (1, 5)), (((8, 12, 16), (10, 14)), (9, 17)), (((22, 18, 24), (20, 26)), (25, 19)))
#gripper model        
#         testModel= ((((0, 2, 4), (6)), (3, 5)), (((14, 10, 8), (12)), (15)), (((16), (18)), (17)))
#depots model        
#         testModel= ((((14, 16, 18, 4, 8), (0, 10)), (5, 9, 15)), (((20), (22)), (21)), (((40, 48, 56), (44, 52, 54, 58)), (41, 49)), (((62, 66, 68, 70), (63, 71)), (60)), (((28, 34, 36), (30, 38)), (29)))

#identify the number of predicates in the vector that we need to create
#start converting each element of the current model into a vector. Create a dictionary which has a vector as a value for each action as the key
            #load the trace here
        with open('src/parking/tRuleFile.txt', 'r') as tRuleFile:
#         with open('src/depots/trules/tRuleGrowthsatellite24jan.txt', 'r') as tRuleFile:            
#         with open('src/depots/trules/newTrulemprime29jan.txt', 'r') as tRuleFile:
#         with open('src/depots/trules/newTruledepots24jan.txt', 'r') as tRuleFile:            
            for line in tRuleFile:
                count=0
#                line=line.replace('-2','')
                currentTrace=line.split('-1')
#this seems to be an error in the code. Reducing the size by 1 at this point to make the correction                
#                 traceLengthArray.append(len(currentTrace))
                traceLengthArray.append(len(currentTrace)-1)
                for currentAction in currentTrace:
                    testOutputVector=[]
                    if((currentAction.strip())!='-2'):
#                         for i in range(len(currentModel)):
#                            predicatesPerActionDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(currentModel[i])))
# #figure out the size of the test model to figure out how much you need to pad it by
#                         vectorLen=len(list(flatten(currentModel)))+len(actionAppearanceOrderDictionary.keys())
#                         vectorLenTestModel=len(list(flatten(testModel)))+len(actionAppearanceOrderDictionary.keys())                        
                        for i in range(len(testModel)):
                           predicatesPerActionTestModelDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(testModel[i])))
                                        
#     #set the dictionary with the rest of the arguments
#                         for k in predicatesPerActionDict.keys():
#                             if(k==currentAction.strip()): 
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(1)
#                             else:
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(0)
 
    #create a dictionary whose keys are the actions and the predicates of the actions, the values can be changed as a function of which action is executing in that trace
    #actually create two dictionaries, one for the list of actions and the other for the predicates of the individual actions
    #the first is for representing the action dictionary
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                actionVectorRepresentationDict[k]=1
                                finalVector.append(1)
                                finalTestVector.append(1)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                finalVector.append(0)
                                finalTestVector.append(0)
                        for i in range(inputVectorLen):
                            finalVector.append(0)  
                            finalTestVector.append(0)
#here set all the bits which correspond to a certain action in actionDictionaryForAllModels to 1, the rest as zero                            
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                applicablePredicates=list(flatten(actionDictionaryForAllModels[k]))
          #                      for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
#the positions of the ones being added change as the action positions must also be accounted for
#                                     finalVector[int(i)]=1
                                    finalVector[int(i)+len(actionAppearanceOrderDictionary.keys())]=1                                    
#set the dictionary with the rest of the arguments for the test model
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                applicablePredicates=list(flatten(testModel[actionAppearanceOrderDictionary[k]]))
#for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
#                                     finalTestVector[int(i)]= 1
                                    finalTestVector[int(i)+len(actionAppearanceOrderDictionary.keys())]= 1
                        entireTraceAsTestVectorList.append(np.array(finalTestVector))
                        entireTraceAsVectorList.append(np.array(finalVector))
                        finalVector=[]
                        finalTestVector=[]
#training output in the form of the succeeding action can be mentioned here
#                        tempCurrentTrace=copy.copy(currentTrace)
                        for k in actionAppearanceOrderDictionary.keys():
#                            if(count>=(len(currentTrace)-2)):
                            if(count>=(len(currentTrace)-1)):
                                break
#                            if(k==currentTrace[currentTrace.index(currentAction)+1].strip()):
                            if(k==currentTrace[count+1].strip()):
#                            if(k==tempCurrentTrace[tempCurrentTrace.index(currentAction)+1].strip()):
                                actionVectorRepresentationDict[k]=1
                                testOutputVector.append(1)
#                                tempCurrentTrace.remove(currentAction)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                testOutputVector.append(0)
#                        trainingOutputVector.append(list(actionVectorRepresentationDict.values()))
                        if testOutputVector:
                            trainingOutputVector.append(np.array(testOutputVector))
                        count=count+1
                entireFileAsVectorList.append(entireTraceAsVectorList)
#combine all the traces for the test vector
                entireFileAsTestVectorList.append(entireTraceAsTestVectorList)  
                entireFileOutputVectorList.append(trainingOutputVector)
                trainingOutputVector=[]
                entireTraceAsVectorList=[] 
                entireTraceAsTestVectorList=[]         
        outputVectorLen=len(actionAppearanceOrderDictionary.keys())
        entireTraceAsVectorList, trainingOutputVector=padTraceToTemp(entireFileAsVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
#create the set of testing vector for the input
        entireTraceAsTestVectorList, trainingOutputVector=padTraceToTemp(entireFileAsTestVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
#         for i in entireTraceAsVectorList:
#             print ('entireTraceAsVectorList',i, len(i))
#         for j in entireTraceAsTestVectorList:
#             print ('entireTraceAsTestVectorList',j, len(j))
#         for k in trainingOutputVector:
#             print('trainingOutputVector',k)
#         print('max(traceLengthArray)', traceLengthArray)
        return entireTraceAsVectorList,entireTraceAsTestVectorList, trainingOutputVector, max(traceLengthArray), inputVectorLen+len(list(actionDictionaryForAllModels.keys()))                 

def operatorSpecificEncodingScheme(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, currentModel):
    print('actionDictForAllModels: ', actionDictionaryForAllModels)
    print('currentModel: ', currentModel)
#     actionDictForAllModels =  {'unload': [{1: "at['cra0 - crate', 'dis0 - distributor']-unload", 6: "at['tru0 - truck', 'dis0 - distributor']-unload", 7: "available['hoi0 - hoist']-unload", 8: "clear['cra0 - crate']-unload", 10: "in['cra0 - crate', 'tru0 - truck']-unload", 11: "lifting['hoi0 - hoist', 'cra0 - crate']-unload"}, {19: "not(at['tru0 - truck', 'dis0 - distributor']-unload)", 20: "not(available['hoi0 - hoist']-unload)", 21: "not(clear['cra0 - crate']-unload)", 23: "not(in['cra0 - crate', 'tru0 - truck']-unload)", 24: "not(lifting['hoi0 - hoist', 'cra0 - crate']-unload)", 14: "not(at['cra0 - crate', 'dis0 - distributor']-unload)"}], 'load': [{1: "at['cra0 - crate', 'dis0 - distributor']-load", 6: "at['tru0 - truck', 'dis0 - distributor']-load", 7: "available['hoi0 - hoist']-load", 8: "clear['cra0 - crate']-load", 10: "in['cra0 - crate', 'tru0 - truck']-load", 11: "lifting['hoi0 - hoist', 'cra0 - crate']-load"}, {19: "not(at['tru0 - truck', 'dis0 - distributor']-load)", 20: "not(available['hoi0 - hoist']-load)", 21: "not(clear['cra0 - crate']-load)", 23: "not(in['cra0 - crate', 'tru0 - truck']-load)", 24: "not(lifting['hoi0 - hoist', 'cra0 - crate']-load)", 14: "not(at['cra0 - crate', 'dis0 - distributor']-load)"}], 'drive': [{4: "at['tru0 - truck', 'dep0 - depot']-drive", 5: "at['tru0 - truck', 'dep1 - depot']-drive", 6: "at['tru0 - truck', 'dis0 - distributor']-drive"}, {17: "not(at['tru0 - truck', 'dep0 - depot']-drive)", 18: "not(at['tru0 - truck', 'dep1 - depot']-drive)", 19: "not(at['tru0 - truck', 'dis0 - distributor']-drive)"}], 'lift': [{1: "at['cra0 - crate', 'dis0 - distributor']-lift", 3: "at['cra1 - crate', 'dis0 - distributor']-lift", 7: "available['hoi0 - hoist']-lift", 8: "clear['cra0 - crate']-lift", 9: "clear['cra1 - crate']-lift", 11: "lifting['hoi0 - hoist', 'cra0 - crate']-lift", 12: "lifting['hoi0 - hoist', 'cra1 - crate']-lift", 29: "on['cra1 - crate', 'pal1 - pallet']-lift", 28: "on['cra0 - crate', 'pal0 - pallet']-lift"}, {16: "not(at['cra1 - crate', 'dis0 - distributor']-lift)", 20: "not(available['hoi0 - hoist']-lift)", 21: "not(clear['cra0 - crate']-lift)", 22: "not(clear['cra1 - crate']-lift)", 24: "not(lifting['hoi0 - hoist', 'cra0 - crate']-lift)", 25: "not(lifting['hoi0 - hoist', 'cra1 - crate']-lift)", 26: "not(on['cra0 - crate', 'pal0 - pallet']-lift)", 27: "not(on['cra1 - crate', 'pal1 - pallet']-lift)", 14: "not(at['cra0 - crate', 'dis0 - distributor']-lift)"}], 'drop': [{0: "at['cra0 - crate', 'dep0 - depot']-drop", 2: "at['cra1 - crate', 'dep1 - depot']-drop", 7: "available['hoi0 - hoist']-drop", 8: "clear['cra0 - crate']-drop", 9: "clear['cra1 - crate']-drop", 11: "lifting['hoi0 - hoist', 'cra0 - crate']-drop", 12: "lifting['hoi0 - hoist', 'cra1 - crate']-drop", 29: "on['cra1 - crate', 'pal1 - pallet']-drop", 28: "on['cra0 - crate', 'pal0 - pallet']-drop"}, {20: "not(available['hoi0 - hoist']-drop)", 21: "not(clear['cra0 - crate']-drop)", 22: "not(clear['cra1 - crate']-drop)", 24: "not(lifting['hoi0 - hoist', 'cra0 - crate']-drop)", 25: "not(lifting['hoi0 - hoist', 'cra1 - crate']-drop)", 26: "not(on['cra0 - crate', 'pal0 - pallet']-drop)", 27: "not(on['cra1 - crate', 'pal1 - pallet']-drop)", 13: "not(at['cra0 - crate', 'dep0 - depot']-drop)", 15: "not(at['cra1 - crate', 'dep1 - depot']-drop)"}]}
#     currentModel = ([["(pre:at['tru0 - truck', 'dis0 - distributor']-load)", "(pre:at['cra0 - crate', 'dis0 - distributor']-load)", "(pre:lifting['hoi0 - hoist', 'cra0 - crate']-load)", "in['cra0 - crate', 'tru0 - truck']-load", "available['hoi0 - hoist']-load", "not(lifting['hoi0 - hoist', 'cra0 - crate']-load)"]], [["(pre:at['cra0 - crate', 'dis0 - distributor']-unload)", "(pre:at['tru0 - truck', 'dis0 - distributor']-unload)", "(pre:available['hoi0 - hoist']-unload)", "(pre:in['cra0 - crate', 'tru0 - truck']-unload)", "not(in['cra0 - crate', 'tru0 - truck']-unload)", "not(available['hoi0 - hoist']-unload)", "lifting['hoi0 - hoist', 'cra0 - crate']-unload"]], [["(pre:at['tru0 - truck', 'dep0 - depot']-drive", "at['tru0 - truck', 'dep1 - depot']-drive", "not(at['tru0 - truck', 'dep0 - depot']-drive)"]], [["(pre:at['cra0 - crate', 'dis0 - distributor']-lift)", "(pre:at['cra1 - crate', 'dis0 - distributor']-lift)", "(pre:available['hoi0 - hoist']-lift)", "(pre:on['cra1 - crate', 'pal1 - pallet']-lift)", "(pre:clear['cra0 - crate']-lift)", "lifting['hoi0 - hoist', 'cra0 - crate']-lift", "clear['cra1 - crate']-lift", "not(at['cra1 - crate', 'dis0 - distributor']-lift)", "not(clear['cra0 - crate']-lift)", "not(available['hoi0 - hoist']-lift)", "not(on['cra1 - crate', 'pal1 - pallet']-lift)"]], [["(pre:at['cra0 - crate', 'dep0 - depot']-drop)", "(pre:at['cra1 - crate', 'dep1 - depot']-drop)", "(pre:clear['cra0 - crate']-drop)", "(pre:lifting['hoi0 - hoist', 'cra0 - crate']-drop)", "available['hoi0 - hoist']-drop", "at['cra1 - crate', 'dep1 - depot']-drop", "clear['cra1 - crate']-drop", "on['cra1 - crate', 'pal1 - pallet']-drop", "not(lifting['hoi0 - hoist', 'cra0 - crate']-drop)", "not(clear['cra0 - crate']-drop)"]])

    actionAppearanceOrderDictionary, predicatesPerActionDictionaryCount, finalVectorDictionary = {}, {}, {}
    finalVector, finalVectorTrace, finalTestVector, trainingOutputVector, testOutputVector, finalVectorCorpus = [], [], [], [], [], []
    operatorNum, finalOutputVectorTrace, finalOutputVectorCorpus = [],[],[]
    finalTestVectorTrace, finalTestVectorCorpus = [],[]
    currentStatePredicates, nextStatePredicates = set(), set()
    #create a dictionary to match the position in the vector with the number labeling the predicate in the actionDictionaryForAllModels
    positionPredicateNumberMatchDict = dict()
    #find the maximum number of operators in all of the traces combined
    for y in range(len(allGeneralizedTraceList)):
        operatorIndices = [j for j, x in enumerate(allGeneralizedTraceList[y]) if x[0] == 'operator']
        operatorNum.append(len(operatorIndices))
    maxOperatorLen = max(operatorNum)
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())+len(actionDictionaryForAllModels[key][1].keys())
    inputVectorLen = sum(predicatesPerActionDictionaryCount.values()) + len(list(actionDictionaryForAllModels.keys()))
    for y in range(inputVectorLen):
        finalVector.append(0)  
        finalTestVector.append(0)
    # assign all the predicates in the actionDictionaryForAllModels to positions in the input vector
    # extract all the predicates from the actionDictionaryForAllModels
    allPredicateList = [i for i in flatten([list(actionDictionaryForAllModels[key][0].values()) + list(actionDictionaryForAllModels[key][1].values()) for key in actionDictionaryForAllModels.keys()])]
    currentModel = [i for i in flatten((currentModel))]
    count = 0
    for y in allPredicateList:
        finalVectorDictionary[len(actionAppearanceOrderDictionary.keys()) + count] = y
        count = count + 1
    #the test vector can be encoded here before the loop as the representation always remains the same
#     for kk in currentModel:
#         if 'pre' in kk: kk = kk.replace('(pre:','').replace(')','')
#         #find index of the current predicate in the vector
#         predicateIndex = [k for (k, v) in finalVectorDictionary.items() if v == kk]
#         print('kk', kk)
#         print('predicateIndex', predicateIndex[0])
#         finalTestVector[predicateIndex[0]] = 1
    for i in range(len(allGeneralizedTraceList)):
        print('at record number: ', i)
        operatorIndices = [q for q, x in enumerate(allGeneralizedTraceList[i]) if x[0] == 'operator']
        predicateIndices = [qq for qq, x in enumerate(allGeneralizedTraceList[i]) if x[0] == 'state']
        for j in range(1,len(allGeneralizedTraceList[i])):
            if 'operator' in allGeneralizedTraceList[i][j]:
                for y in range((inputVectorLen)):
                    finalVector[y] = 0  
#                     finalTestVector.append(0)
                currentAction = allGeneralizedTraceList[i][j][1]
                #continue as long as the last action is not reached
                if (operatorIndices.index(j))!=(len(operatorIndices)-1):
                    nextAction = allGeneralizedTraceList[i][operatorIndices[operatorIndices.index(j)+1]][1]
                    nextActionIndex = operatorIndices[operatorIndices.index(j)+1]
                previousActionIndex = operatorIndices[operatorIndices.index(j)-1]
                #find the indices between the current operator and the previous one
                currentStateIndices = [k for k in predicateIndices if k<j and k>previousActionIndex]
                nextStateIndices = [k for k in predicateIndices if k>j and k<nextActionIndex]                
                for x in currentStateIndices:
                    #Create a set to to eleminate the number of duplicates in the predicates of the state 
                    currentStatePredicates.add(allGeneralizedTraceList[i][x][1])
                for x in nextStateIndices:
                    #Create a set to to eleminate the number of duplicates in the predicates of the state 
                    nextStatePredicates.add(allGeneralizedTraceList[i][x][1])
                #find the difference between the current and next state sets to figure out the predicates that are added
                currentNextStateDifference = nextStatePredicates.difference(currentStatePredicates) 
                nextState = allGeneralizedTraceList[i][j+2]
                #encode the current action into the vector form
                for k in actionAppearanceOrderDictionary.keys():
                    if(k==currentAction):
#                         actionVectorRepresentationDict[k]=1
                        finalVector[actionAppearanceOrderDictionary[k]] = 1
                        finalTestVector[actionAppearanceOrderDictionary[k]] = 1
                    else:   
#                         actionVectorRepresentationDict[k]=0
                        finalVector[actionAppearanceOrderDictionary[k]] = 0
                        finalTestVector[actionAppearanceOrderDictionary[k]] = 0

                #encode the next action into the vector form
                for k in actionAppearanceOrderDictionary.keys():
                    if(k==nextAction):
#                         actionVectorRepresentationDict[k]=1
                        trainingOutputVector.insert(actionAppearanceOrderDictionary[k], 1)
#                         testOutputVector.insert(actionAppearanceOrderDictionary[k], 1)
                    else:   
#                         actionVectorRepresentationDict[k]=0
                        trainingOutputVector.insert(actionAppearanceOrderDictionary[k], 0)
#                         testOutputVector.insert(actionAppearanceOrderDictionary[k], 0)

                #Find out which predicates from the current state are relevant to the action being applied.
                for t in currentStatePredicates:
                    for tt in actionDictionaryForAllModels[currentAction][0].values():
                        if t in tt:
#                             predicateIndex = [k for (k, v) in actionDictionaryForAllModels[currentAction][0].items() if v == tt]
                            predicateIndex = [k for (k, v) in finalVectorDictionary.items() if v == tt]
                            #find the number of indices that the actions before the current action occupy
#                             positionInOrder = list(actionAppearanceOrderDictionary.keys()).index(currentAction)
#                             indiceShift = 0
#                             if positionInOrder>0: 
#                                 for d in range(positionInOrder):
#                                     actionAtPosition = [k for (k, v) in actionAppearanceOrderDictionary.items() if v == d]
#                                     indiceShift = indiceShift + predicatesPerActionDictionaryCount[actionAtPosition[0]]   
                            #bring the index to the beginning of the correct block
#                             finalVector[indiceShift + predicateIndex[0]+ len(list(actionDictionaryForAllModels.keys()))] = 1
                            finalVector[predicateIndex[0]] = 1
                            break                            
                            
                #Find out which predicates from the state difference between the current and next state are relevant to the action being applied.
                for t in currentNextStateDifference:
                    for tt in actionDictionaryForAllModels[currentAction][0].values():
                        if t in tt:
#                             predicateIndex = [k for (k, v) in actionDictionaryForAllModels[currentAction][0].items() if v == tt]
                            predicateIndex = [k for (k, v) in finalVectorDictionary.items() if v == tt]
                            #find the number of indices that the actions before the current action occupy
#                             positionInOrder = list(actionAppearanceOrderDictionary.keys()).index(currentAction)
#                             indiceShift = 0
#                             if positionInOrder>0: 
#                                 for d in range(positionInOrder):
#                                     actionAtPosition = [k for (k, v) in actionAppearanceOrderDictionary.items() if v == d]
#                                     indiceShift = indiceShift + predicatesPerActionDictionaryCount[actionAtPosition[0]]   
                            #bring the index to the beginning of the correct block
#                             finalVector[indiceShift + predicateIndex[0]+ len(list(actionDictionaryForAllModels.keys()))] = 1
                            finalVector[predicateIndex[0]] = 1 
                            break                           
                #switch the predicates corresponding to the current action for the current model being tested as 1
                currentPredicatesTestModel = [kk for kk in currentModel if currentAction in kk]
                for yy in currentPredicatesTestModel:
                    if 'pre' in yy: yy = yy.replace('(pre:','').replace(')','')
                    #find index of the current predicate in the vector
                    predicateIndexTest = [k for (k, v) in finalVectorDictionary.items() if v == yy]
                    if predicateIndexTest: finalTestVector[predicateIndexTest[0]] = 1
#                 for key in numericPredicateDictionary.keys():
#                     positionInOrder = list(actionAppearanceOrderDictionary.keys()).index(key)
#                     indiceShift = 0
#                     if positionInOrder>0:
#                         for d in range(positionInOrder):
#                           indiceShift = indiceShift + list(actionAppearanceOrderDictionary.values())[d]
#                     for zz in numericPredicateDictionary[key]:
#                         finalTestVector[indiceShift + int(zz[0])+ len(list(actionDictionaryForAllModels.keys()))] = 1
#                 print(finalVectorTrace)
#                 currentModel = [i for i in flatten(list(currentModel))]
                finalVectorTrace.append(np.array(finalVector))
                finalOutputVectorTrace.append(np.array(trainingOutputVector))
                finalTestVectorTrace.append(np.array(finalTestVector))                
#                 finalVector = []
#                 finalTestVector = []
                trainingOutputVector = []
        finalVectorCorpus.append(finalVectorTrace)
        finalTestVectorCorpus.append(finalTestVectorTrace)
        finalOutputVectorCorpus.append(finalOutputVectorTrace)
        #pad the input and output vector corpus to the certain max length of the entire batch
        finalVectorTrace = []
        finalTestVectorTrace = []
        finalOutputVectorTrace = [] 
    finalTestOutputVectorCorpus = copy.copy(finalOutputVectorCorpus)  
    finalVectorCorpus, finalOutputVectorCorpus = padTraceToTemp(finalVectorCorpus, maxOperatorLen, inputVectorLen, finalOutputVectorCorpus, len(list(actionAppearanceOrderDictionary.keys())))
    finalTestVectorCorpus, finalTestOutputVectorCorpus = padTraceToTemp(finalTestVectorCorpus, maxOperatorLen, inputVectorLen, finalTestOutputVectorCorpus, len(list(actionAppearanceOrderDictionary.keys())))
#     pickle.dump(finalVectorCorpus, open("src/learning/modelGeneration/generated/corpusDump/gripper_trainIp", "wb"))
#     pickle.dump(finalOutputVectorCorpus, open("src/learning/modelGeneration/generated/corpusDump/gripper_trainOp", "wb"))
    return finalVectorCorpus,finalTestVectorCorpus, finalOutputVectorCorpus, maxOperatorLen, inputVectorLen

#function which encodes the HRI traces into vector format and performs training as well as testing with the corrected handwoven model
def  trainTestCorrectedModelHRI():
    traceList,entireFileOutputVectorList, entireFile, predicatesPerAction, preconditionList, addconditionList, postconditionList, entireTraceAsTestVectorList, entireFileAsTestVectorList=[],[],[],[],[],[],[],[],[]
    predicatesPerActionDictionaryCount, predicatesPerActionTestModelDict={},{}
    actionDictionaryForAllModels={}
    actionAppearanceOrderDictionary={}
    predicatesPerActionDict={}
    actionVectorRepresentationDict={}
    predicateVectorRepresentationDict={}
    count=0
    entireFileAsVectorList, trainingOutputVector, entireActionAsVectorList, entireFileAsVector, traceLengthArray, entireTraceAsVectorList, finalVector, postconditionList, finalTestVector=[],[],[],[],[],[],[],[],[]
    #open file which contains cartesian product of all the models and pick them up one by one
#     with open('src/parking/allPredicateNumberDictionary', 'r') as file:
#     with open('src/depots/dictionaries/allPredicateNumberDictionarysatellite28jan.txt', 'r') as file:
#     with open('src/depots/dictionaries/allPredicateNumberDictionarymprime.txt', 'r') as file:        
#     with open('src/depots/dictionaries/allPredicateNumberDictionarygripper.txt', 'r') as file:
#     with open('src/depots/dictionaries/allPredicateNumberDictionarydepots.txt', 'r') as file:
    with open('src/hri/dictionaries/actionDictionaryForAllModels_1', 'r') as file:                
        actionDictionaryForAllModels=json.load(file)
    file.close()
#command to find the current working directory    
#    print(os.getcwd())
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        if(len(actionDictionaryForAllModels[key])==1):predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())
        else:predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys()) + len(actionDictionaryForAllModels[key][1].keys())
    inputVectorLen=sum(predicatesPerActionDictionaryCount.values())       
    allModelFile=open("src/learning/modelGeneration/generated/satellite/allModelFile_satellite","rb")
    itemList=pickle.load(allModelFile)  
    for currentModel in itemList:
#hard code ideal model at this stage as we want to train with the ideal model
        currentModel=((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, 10)), (9, )), (((40, 44, 50), (42,)), (45, )))
#hard code the model to be tested as we want to see how accurately the new model fares in the testing
#parking domain
#         testModel= ((((0,), (2,)), (1,)), (((16,), (18,)), (17,)), (((32,), (34,)), (33,)), (((48,), (50,)), (49,)))
#         testModel= ((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, )), (9, )), (((40, 44, 50), (42,)), (45, )))
#satellite domain
#         testModel= ((((0, ), (1, )), (2, )), (((34, 38), (32, )), (35, )), (((36, 18), (20, )), (17, 19)), (((40, 41, 22, 28), (24,)), ()), (((6, 8, 12, 43, 44), (14, )), ()))
#mprime model        
        testModel = (((401, 393, 397, 399), (395, 394)), ((65, 67, 69, 71), (70)), ((15, 53, 33, 47), (61)), ((215, 253, 247), (261)), ((117, 91, 119), (95)), ((151, 187, 183), (197)), ((279, 317, 311), (325)), ((447, 437, 449), (425)), ((507, 485, 513), (481)), ((369, 345, 375, ), (367)), ((531), ()), ((532), ()), ((533), ()))
#         testModel = (((401, 393, 397, 399), (395, 394)), ((65, 67, 69, 71), (70)), ((15, 53, 33, 47), (61)), ((215, 253, 247), (261)), ((117, 91, 119), (95)), ((151, 187, 183), (197)), ((279, 317, 311), (325)), ((447, 437, 449), (425)), ((507, 485, 513), (481)), ((369, 345, 375, ), (367)))
#gripper model        
#         testModel= ((((0, 2, 4), (6)), (3, 5)), (((14, 10, 8), (12)), (15)), (((16), (18)), (17)))
#depots model        
#         testModel= ((((14, 16, 18, 4, 8), (0, 10)), (5, 9, 15)), (((20), (22)), (21)), (((40, 48, 56), (44, 52, 54, 58)), (41, 49)), (((62, 66, 68, 70), (63, 71)), (60)), (((28, 34, 36), (30, 38)), (29)))

#identify the number of predicates in the vector that we need to create
#start converting each element of the current model into a vector. Create a dictionary which has a vector as a value for each action as the key
            #load the trace here
        with open('src/hri/learning/sequenceCombined.txt', 'r') as tRuleFile:
#         with open('src/depots/trules/tRuleGrowthsatellite24jan.txt', 'r') as tRuleFile:            
#         with open('src/depots/trules/newTrulemprime29jan.txt', 'r') as tRuleFile:
#         with open('src/depots/trules/newTruledepots24jan.txt', 'r') as tRuleFile:            
            for line in tRuleFile:
                count=0
#                line=line.replace('-2','')
                currentTrace=line.split('-1')
#this seems to be an error in the code. Reducing the size by 1 at this point to make the correction                
#                 traceLengthArray.append(len(currentTrace))
                traceLengthArray.append(len(currentTrace)-1)
                for currentAction in currentTrace:
                    testOutputVector=[]
                    if((currentAction.strip())!='-2'):
#                         for i in range(len(currentModel)):
#                            predicatesPerActionDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(currentModel[i])))
# #figure out the size of the test model to figure out how much you need to pad it by
#                         vectorLen=len(list(flatten(currentModel)))+len(actionAppearanceOrderDictionary.keys())
#                         vectorLenTestModel=len(list(flatten(testModel)))+len(actionAppearanceOrderDictionary.keys())                        
                        for i in range(len(testModel)):
                            predicatesPerActionTestModelDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(testModel[i])))
                                        
#     #set the dictionary with the rest of the arguments
#                         for k in predicatesPerActionDict.keys():
#                             if(k==currentAction.strip()): 
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(1)
#                             else:
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(0)
 
    #create a dictionary whose keys are the actions and the predicates of the actions, the values can be changed as a function of which action is executing in that trace
    #actually create two dictionaries, one for the list of actions and the other for the predicates of the individual actions
    #the first is for representing the action dictionary
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                actionVectorRepresentationDict[k]=1
                                finalVector.append(1)
                                finalTestVector.append(1)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                finalVector.append(0)
                                finalTestVector.append(0)
                        for i in range(inputVectorLen):
                            finalVector.append(0)  
                            finalTestVector.append(0)
#here set all the bits which correspond to a certain action in actionDictionaryForAllModels to 1, the rest as zero                            
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                #modification being done in the case of the HRI scenario as the training and testing models are the same.
#                                 applicablePredicates=list(flatten(actionDictionaryForAllModels[k]))
                                applicablePredicates=list(flatten(testModel[actionAppearanceOrderDictionary[k]]))
          #                      for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
#the positions of the ones being added change as the action positions must also be accounted for
#                                     finalVector[int(i)]=1
#                                     print('key',k, 'sum:',int(i)+len(actionAppearanceOrderDictionary.keys()) )
                                    finalVector[int(i)]=1                                    
#set the dictionary with the rest of the arguments for the test model
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                applicablePredicates=list(flatten(testModel[actionAppearanceOrderDictionary[k]]))
#for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
#                                     finalTestVector[int(i)]= 1
                                    finalTestVector[int(i)]= 1
                        entireTraceAsTestVectorList.append(np.array(finalTestVector))
                        entireTraceAsVectorList.append(np.array(finalVector))
                        finalVector=[]
                        finalTestVector=[]
#training output in the form of the succeeding action can be mentioned here
#                        tempCurrentTrace=copy.copy(currentTrace)
                        for k in actionAppearanceOrderDictionary.keys():
#                            if(count>=(len(currentTrace)-2)):
                            if(count>=(len(currentTrace)-1)):
                                break
#                            if(k==currentTrace[currentTrace.index(currentAction)+1].strip()):
                            if(k==currentTrace[count+1].strip()):
#                            if(k==tempCurrentTrace[tempCurrentTrace.index(currentAction)+1].strip()):
                                actionVectorRepresentationDict[k]=1
                                testOutputVector.append(1)
#                                tempCurrentTrace.remove(currentAction)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                testOutputVector.append(0)
#                        trainingOutputVector.append(list(actionVectorRepresentationDict.values()))
                        if testOutputVector:
                            trainingOutputVector.append(np.array(testOutputVector))
                        count=count+1
                entireFileAsVectorList.append(entireTraceAsVectorList)
#combine all the traces for the test vector
                entireFileAsTestVectorList.append(entireTraceAsTestVectorList)  
                entireFileOutputVectorList.append(trainingOutputVector)
                trainingOutputVector=[]
                entireTraceAsVectorList=[] 
                entireTraceAsTestVectorList=[]         
        outputVectorLen=len(actionAppearanceOrderDictionary.keys())
        entireTraceAsVectorList, trainingOutputVector=padTraceToTemp(entireFileAsVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
#create the set of testing vector for the input
        entireTraceAsTestVectorList, trainingOutputVector=padTraceToTemp(entireFileAsTestVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
#         for i in entireTraceAsVectorList:
#             print ('entireTraceAsVectorList',i, len(i))
#         for j in entireTraceAsTestVectorList:
#             print ('entireTraceAsTestVectorList',j, len(j))
#         for k in trainingOutputVector:
#             print('trainingOutputVector',k)
#         print('max(traceLengthArray)', traceLengthArray)
        return entireTraceAsVectorList,entireTraceAsTestVectorList, trainingOutputVector, max(traceLengthArray), inputVectorLen+len(list(actionDictionaryForAllModels.keys()))                 


#function which converts an action into a vector
def  convert_data_to_vectorTemp(filename):
    traceList,entireFileOutputVectorList, entireFile, predicatesPerAction, preconditionList, addconditionList, postconditionList, entireTraceAsTestVectorList, entireFileAsTestVectorList=[],[],[],[],[],[],[],[],[]
    predicatesPerActionDictionaryCount, predicatesPerActionTestModelDict={},{}
    actionDictionaryForAllModels={}
    actionAppearanceOrderDictionary={}
    predicatesPerActionDict={}
    actionVectorRepresentationDict={}
    predicateVectorRepresentationDict={}
    count=0
    entireFileAsVectorList, trainingOutputVector, entireActionAsVectorList, entireFileAsVector, traceLengthArray, entireTraceAsVectorList, finalVector, postconditionList, finalTestVector=[],[],[],[],[],[],[],[],[]
    #open file which contains cartesian product of all the models and pick them up one by one
    with open('src/parking/allPredicateNumberDictionary', 'r') as file:
        actionDictionaryForAllModels=json.load(file)
    file.close()
#command to find the current working directory    
#    print(os.getcwd())
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())+len(actionDictionaryForAllModels[key][1].keys())
    inputVectorLen=sum(predicatesPerActionDictionaryCount.values())       
    allModelFile=open("src/parking/allModelFile","rb")
    itemList=pickle.load(allModelFile)  
    for currentModel in itemList:
#hard code ideal model at this stage as we want to train with the ideal model
        currentModel=((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, 10)), (9, )), (((40, 44, 50), (42,)), (45, )))
#hard code the model to be tested as we want to see how accurately the new model fares in the testing
#         testModel= ((((0,), (2,)), (1,)), (((16,), (18,)), (17,)), (((32,), (34,)), (33,)), (((48,), (50,)), (49,)))
        testModel= ((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, )), (9, )), (((40, 44, 50), (42,)), (45, )))
        print(currentModel)
#identify the number of predicates in the vector that we need to create
#start converting each element of the current model into a vector. Create a dictionary which has a vector as a value for each action as the key
            #load the trace here
        with open('src/parking/tRuleFile.txt', 'r') as tRuleFile:
            for line in tRuleFile:
                count=0
#                line=line.replace('-2','')
                currentTrace=line.split('-1')
                traceLengthArray.append(len(currentTrace))
                for currentAction in currentTrace:
                    testOutputVector=[]
                    if((currentAction.strip())!='-2'):
                        for i in range(len(currentModel)):
                           predicatesPerActionDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(currentModel[i])))
#figure out the size of the test model to figure out how much you need to pad it by
                        vectorLen=len(list(flatten(currentModel)))+len(actionAppearanceOrderDictionary.keys())
                        vectorLenTestModel=len(list(flatten(testModel)))+len(actionAppearanceOrderDictionary.keys())                        
                        for i in range(len(testModel)):
                           predicatesPerActionTestModelDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(testModel[i])))
                                        
#     #set the dictionary with the rest of the arguments
#                         for k in predicatesPerActionDict.keys():
#                             if(k==currentAction.strip()): 
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(1)
#                             else:
#                                 for i in range((predicatesPerActionDict[k])):
#                                     finalVector.append(0)
 
    #create a dictionary whose keys are the actions and the predicates of the actions, the values can be changed as a function of which action is executing in that trace
    #actually create two dictionaries, one for the list of actions and the other for the predicates of the individual actions
    #the first is for representing the action dictionary
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                actionVectorRepresentationDict[k]=1
                                finalVector.append(1)
                                finalTestVector.append(1)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                finalVector.append(0)
                                finalTestVector.append(0)
                        for i in range(inputVectorLen):
                            finalVector.append(0)  
                            finalTestVector.append(0)
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                applicablePredicates=list(flatten(currentModel[actionAppearanceOrderDictionary[k]]))
          #                      for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
                                    finalVector[int(i)]=1
#set the dictionary with the rest of the arguments for the test model
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                applicablePredicates=list(flatten(testModel[actionAppearanceOrderDictionary[k]]))
#for i in (list(actionDictionaryForAllModels[k][0].keys())+list(actionDictionaryForAllModels[k][1].keys())):
                                for i in applicablePredicates:
                                    finalTestVector[int(i)]= 1
                        entireTraceAsTestVectorList.append(np.array(finalTestVector))
                        entireTraceAsVectorList.append(np.array(finalVector))
                        finalVector=[]
                        finalTestVector=[]
#training output in the form of the succeeding action can be mentioned here
#                        tempCurrentTrace=copy.copy(currentTrace)
                        for k in actionAppearanceOrderDictionary.keys():
#                            if(count>=(len(currentTrace)-2)):
                            if(count>=(len(currentTrace)-1)):
                                break
#                            if(k==currentTrace[currentTrace.index(currentAction)+1].strip()):
                            if(k==currentTrace[count+1].strip()):
#                            if(k==tempCurrentTrace[tempCurrentTrace.index(currentAction)+1].strip()):
                                actionVectorRepresentationDict[k]=1
                                testOutputVector.append(1)
#                                tempCurrentTrace.remove(currentAction)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                testOutputVector.append(0)
#                        trainingOutputVector.append(list(actionVectorRepresentationDict.values()))
                        if testOutputVector:
                            trainingOutputVector.append(np.array(testOutputVector))
                        count=count+1
                entireFileAsVectorList.append(entireTraceAsVectorList)
#combine all the traces for the test vector
                entireFileAsTestVectorList.append(entireTraceAsTestVectorList)  
                entireFileOutputVectorList.append(trainingOutputVector)
                trainingOutputVector=[]
                entireTraceAsVectorList=[] 
                entireTraceAsTestVectorList=[]         
        outputVectorLen=len(actionAppearanceOrderDictionary.keys())
        entireTraceAsVectorList, trainingOutputVector=padTraceToTemp(entireFileAsVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
#create the set of testing vector for the input
        entireTraceAsTestVectorList, trainingOutputVector=padTraceToTemp(entireFileAsTestVectorList,max(traceLengthArray),inputVectorLen+len(list(actionDictionaryForAllModels.keys())), entireFileOutputVectorList, outputVectorLen)
        return entireTraceAsVectorList,entireTraceAsTestVectorList, trainingOutputVector, max(traceLengthArray), inputVectorLen+len(list(actionDictionaryForAllModels.keys()))                 


#function which converts an action into a vector
def  convert_data_to_vector(filename):
    traceList,entireFileOutputVectorList, entireFile, predicatesPerAction, preconditionList, addconditionList, postconditionList, entireTraceAsTestVectorList, entireFileAsTestVectorList=[],[],[],[],[],[],[],[],[]
    predicatesPerActionDictionaryCount, predicatesPerActionTestModelDict={},{}
    actionDictionaryForAllModels={}
    actionAppearanceOrderDictionary={}
    predicatesPerActionDict={}
    actionVectorRepresentationDict={}
    predicateVectorRepresentationDict={}
    count=0
    entireFileAsVectorList, trainingOutputVector, entireActionAsVectorList, entireFileAsVector, traceLengthArray, entireTraceAsVectorList, finalVector, postconditionList, finalTestVector=[],[],[],[],[],[],[],[],[]
    #open file which contains cartesian product of all the models and pick them up one by one
    with open('src/parking/allPredicateNumberDictionary', 'r') as file:
        actionDictionaryForAllModels=json.load(file)
    file.close()
#command to find the current working directory    
#    print(os.getcwd())
    for key in actionDictionaryForAllModels.keys():
        actionAppearanceOrderDictionary[key]=list(actionDictionaryForAllModels.keys()).index(key.strip())
        predicatesPerActionDictionaryCount[key]=len(actionDictionaryForAllModels[key][0].keys())+len(actionDictionaryForAllModels[key][1].keys())
                
    allModelFile=open("src/parking/allModelFile","rb")
    itemList=pickle.load(allModelFile)  
    for currentModel in itemList:
        numberOfActions=len(currentModel)
#hard code ideal model at this stage as we want to train with the ideal model
        currentModel=((((32, 36, 28, ), (34, 30, )), (37, 29)), (((18, 20, 16, 26), (22, )), (21, 17, 25)), (((4, 8), (0, 6, 10)), (9, )), (((40, 44, 50), (42,)), (45, )))
#hard code the model to be tested as we want to see how accurately the new model fares in the testing
        testModel= ((((0,), (2,)), (1,)), (((16,), (18,)), (17,)), (((32,), (34,)), (33,)), (((48,), (50,)), (49,)))
        print(currentModel)
        for i in range(len(currentModel)):
            numberReplacedWithPredicateDict=len(((currentModel[i])[0])[0])+len(((currentModel[i])[0])[1])+len((currentModel[i])[1])
#identify the number of predicates in the vector that we need to create
#start converting each element of the current model into a vector. Create a dictionary which has a vector as a value for each action as the key
            #load the trace here
        with open('src/parking/tRuleFileCopy2.txt', 'r') as tRuleFile:
            for line in tRuleFile:
                count=0
#                line=line.replace('-2','')
                currentTrace=line.split('-1')
                traceLengthArray.append(len(currentTrace))
                for currentAction in currentTrace:
                    testOutputVector=[]
                    if((currentAction.strip())!='-2'):
    #the current block of code is eleminated because we don't need the names of the predicates, just the number of predicates is enough
                        numberPredicateList=actionDictionaryForAllModels[currentAction.strip()]
    #get separate predicates for the pre, add and delete lists
    #                    for j in range(len(((currentModel[i])[0])[0])):
    #                         preconditionList.append(((actionDictionaryForAllModels[currentAction.strip()])[0])[str((((currentModel[i])[0])[0])[j])])
    #                    for j in range(len(((currentModel[i])[0])[1])):
    #                         addconditionList.append(((actionDictionaryForAllModels[currentAction.strip()])[0])[str((((currentModel[i])[0])[1])[j])])
    #                    for j in range(len(((currentModel[i])[1]))):
    #                         postconditionList.append(((actionDictionaryForAllModels[currentAction.strip()])[1])[str(((currentModel[i])[1])[j])])
                        for i in range(len(currentModel)):
                           predicatesPerActionDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(currentModel[i])))
#we hardcode this dictionary at the moment as we want to train with the ideal model and test with a non ideal one
                        predicatesPerActionDict={'move-curb-to-curb': 7, 'move-curb-to-car': 8,  'move-car-to-curb': 6,  'move-car-to-car': 5}
#figure out the size of the test model to figure out how much you need to pad it by
                        vectorLen=len(list(flatten(currentModel)))+len(actionAppearanceOrderDictionary.keys())
                        vectorLenTestModel=len(list(flatten(testModel)))+len(actionAppearanceOrderDictionary.keys())                        
                        for i in range(len(testModel)):
                           predicatesPerActionTestModelDict[list(actionAppearanceOrderDictionary.keys())[list(actionAppearanceOrderDictionary.values()).index(i)]]=len(list(flatten(testModel[i])))

    #keep this expression when we need to figure out what action is applied at every point and how it has to be referenced from the action list
    #                   vectorLength=numberPredicateList.length+len(currentModel[actionAppearanceOrderDictionary[currentAction.strip()]])
    #create a dictionary whose keys are the actions and the predicates of the actions, the values can be changed as a function of which action is executing in that trace
    #actually create two dictionaries, one for the list of actions and the other for the predicates of the individual actions
    #the first is for representing the action dictionary
                        for k in actionAppearanceOrderDictionary.keys():
                            if(k==currentAction.strip()):
                                actionVectorRepresentationDict[k]=1
                                finalVector.append(1)
                                finalTestVector.append(1)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                finalVector.append(0)
                                finalTestVector.append(0)
    #set the dictionary with the rest of the arguments
                        for k in predicatesPerActionDict.keys():
                            if(k==currentAction.strip()): 
                                for i in range((predicatesPerActionDict[k])):
                                    finalVector.append(1)
                            else:
                                for i in range((predicatesPerActionDict[k])):
                                    finalVector.append(0)
                        entireTraceAsVectorList.append(np.array(finalVector))
                        finalVector=[]

#set the dictionary with the rest of the arguments for the test model
                        for k in predicatesPerActionTestModelDict.keys():
                            if(k==currentAction.strip()): 
                                for i in range((predicatesPerActionTestModelDict[k])):
                                    finalTestVector.append(1)
                            else:
                                for i in range((predicatesPerActionTestModelDict[k])):
                                    finalTestVector.append(0)
                        for k in range(0,abs(vectorLen-vectorLenTestModel)):
                            finalTestVector.append(0)
                        entireTraceAsTestVectorList.append(np.array(finalTestVector))
                        finalTestVector=[]
                                                
#training output in the form of the succeeding action can be mentioned here
#                        tempCurrentTrace=copy.copy(currentTrace)
                        for k in actionAppearanceOrderDictionary.keys():
#                            if(count>=(len(currentTrace)-2)):
                            if(count>=(len(currentTrace)-1)):
                                break
#                            if(k==currentTrace[currentTrace.index(currentAction)+1].strip()):
                            if(k==currentTrace[count+1].strip()):
#                            if(k==tempCurrentTrace[tempCurrentTrace.index(currentAction)+1].strip()):
                                actionVectorRepresentationDict[k]=1
                                testOutputVector.append(1)
#                                tempCurrentTrace.remove(currentAction)
                            else:   
                                actionVectorRepresentationDict[k]=0
                                testOutputVector.append(0)
#                        trainingOutputVector.append(list(actionVectorRepresentationDict.values()))
                        if testOutputVector:
                            trainingOutputVector.append(np.array(testOutputVector))
                        count=count+1
#                print('len(entireTraceAsVectorList)',len(entireTraceAsVectorList))
#                print('len(trainingOutputVector)',len(trainingOutputVector))                
                entireFileAsVectorList.append(entireTraceAsVectorList)
#combine all the traces for the test vector
                entireFileAsTestVectorList.append(entireTraceAsTestVectorList)  
                entireFileOutputVectorList.append(trainingOutputVector)
                trainingOutputVector=[]
                entireTraceAsVectorList=[] 
                entireTraceAsTestVectorList=[]         
#pad sequence to the maximum length
                         
#           entireFileAsVector.append(entireTraceAsVectorList)                      
                       
#         for i in entireTraceAsVectorList:
#            print(i)
#            print('\n')
       #doing this return at this stage to keep the number of models explored to 1
#         for i in entireTraceAsVectorList:
#             print (i)
#             print('\n')
#        vectorLen=len(list(flatten(currentModel)))+len(actionAppearanceOrderDictionary.keys())
        outputVectorLen=len(actionAppearanceOrderDictionary.keys())
        entireTraceAsVectorList, trainingOutputVector=padTraceToTemp(entireFileAsVectorList,max(traceLengthArray),vectorLen, entireFileOutputVectorList, outputVectorLen)
#create the set of testing vector for the input
        entireTraceAsTestVectorList, trainingOutputVector=padTraceToTemp(entireFileAsTestVectorList,max(traceLengthArray),vectorLen, entireFileOutputVectorList, outputVectorLen)
        return entireTraceAsVectorList,entireTraceAsTestVectorList, trainingOutputVector, max(traceLengthArray), vectorLen                 


def padTraceToMax(entireTraceAsVectorList,maxLen,vectorLen):
    tempList=[]
    for i in entireTraceAsVectorList:
        padNumber=maxLen-len(i)
        for j in range(padNumber):
            for k in range((vectorLen)):
                tempList.append(0)
            i.append(np.asarray(tempList))
    return  entireTraceAsVectorList


def padTraceToTemp(entireTraceAsVectorList,maxLen,vectorLen,entireFileOutputVectorList, outputVectorLen):
    tempEntireFileOutputVectorList=[]
    count=0
    for i in entireTraceAsVectorList:
#         print(i)
        padNumber=maxLen-len(i)
        for j in range(padNumber):
            tempList=[]
            tempOutputVectorList=[]
            for k in range((vectorLen)):
                tempList.append(0)
            for x in range(outputVectorLen):
                tempOutputVectorList.append(0)
            i.append(np.array(tempList))
            entireFileOutputVectorList[count].append(np.array(tempOutputVectorList))
        count=count+1
    flattenedList = [np.array(val) for sublist in entireTraceAsVectorList for val in sublist]
    flattenedOutputVectorList = [val for sublist in entireFileOutputVectorList for val in sublist]
#     for i in flattenedOutputVectorList:
#         print('i', i)
    return  flattenedList,flattenedOutputVectorList


        
def flatten(l):
    for el in l:
        if isinstance(el, collections.Iterable) and not isinstance(el, (str, bytes)):
            yield from flatten(el)
        else:
            yield el


#FORGOT SIGNIFICANCE OF THIS CODE, COMMENTING IT FOR NOW 
# #model wise learning phase begins here. We pickup each model one by one and start the vector creation
#     with open("src/parking/predicatesPerAction","r") as file:
#         predicatesPerActionDictionary=json.load(file)
#         for keyOuter in predicatesPerActionDictionary:
#             predicatesPerActionInner=[]
#             for keyInner in predicatesPerActionDictionary[keyOuter]:
#                 if not keyInner in predicatesPerActionDictionaryCount:
#                     predicatesPerActionDictionaryCount[keyInner]=[len(((predicatesPerActionDictionary[keyOuter])[keyInner]))]
#                 else:
#                     predicatesPerActionDictionaryCount[keyInner].append(len(((predicatesPerActionDictionary[keyOuter])[keyInner])))
# #                 predicatesPerActionInner.append(keyInner)
# #                 predicatesPerActionInner.append(len(((predicatesPerActionDictionary[keyOuter])[keyInner])))
# #             predicatesPerActionDictionaryCount.append(predicatesPerActionInner)
#         file.close()
#     
#     #need to go back into the traces at this time to find out specific actions at each point in time


def ptb_raw_data(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, numericPredicateDictionary):
#   train_path = os.path.join(data_path, "parking.train.txt")
#   valid_path = os.path.join(data_path, "parking.valid.txt")
#   test_path = os.path.join(data_path, "parking.test.txt")
#   train_input, test_input, train_output, maxLen, vectorLen=convert_data_to_vectorTemp(train_path)

#uncomment the line after in the case of the HRI traces
#   train_input, test_input, train_output, maxLen, vectorLen=trainAllPredicateTestModel()
#     train_input, test_input, train_output, maxLen, vectorLen = trainTestCorrectedModelHRI()
  
  #uncomment the line below to introduce the new encoding scheme as of 16 august
    train_input, test_input, train_output, maxLen, vectorLen = operatorSpecificEncodingScheme(predicateTypeDictionary, allGeneralizedTraceList, actionDictionaryForAllModels, numericPredicateDictionary)
#     with open("src/learning/modelGeneration/generated/corpusDump/gripper_trainIp", "rb") as allModelFileBinary:
#         train_input = pickle.load(allModelFileBinary)
#     with open("src/learning/modelGeneration/generated/corpusDump/gripper_trainOp", "rb") as allModelFileBinary1:    
#         train_output = pickle.load(allModelFileBinary1)
#   valid_data, test_data=train_input, train_input
#     print('train_input', len(train_input))
#     print('train_output', len(train_output))
#     print('test_input', len(test_input))
#     for i in train_input: print('len(train_input)', len(i), 'train_input', i)
#     for j in test_input: print('len(test_input)', len(j), 'test_input', j)
#     for k in test_input: print(j)
    
    return train_input, train_output, test_input, maxLen, vectorLen


