(define (problem strips-mprime-l2-f2-s2-v2-c2)
(:domain mprime-strips)
(:objects
 		  f0 - fuel
 		  f1 - fuel
 		  f2 - fuel
 		  s0 - space
 		  s1 - space
 		  s2 - space
          l0 - location
          l1 - location
          v0 - vehicle
          v1 - vehicle
          c0 - cargo
          c1 - cargo
)
(:init
(not-equal l0 l1)
(not-equal l1 l0)
(fuel-neighbor f0 f1)
(fuel-neighbor f1 f2)
(space-neighbor s0 s1)
(space-neighbor s1 s2)
(conn l0 l1)
(conn l1 l0)
(conn l1 l0)
(conn l0 l1)
(has-fuel l0 f0)
(has-fuel l1 f2)
(has-space  v0 s2)
(has-space  v1 s1)
(at v0 l1)
(at v1 l0)
(at c0 l1)
(at c1 l1)
)
(:goal
(and
(at c0 l0)
(at c1 l0)
)
)
)


