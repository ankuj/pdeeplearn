(define (problem gripper-3-3-3)
(:domain gripper-strips)
(:objects
   robot1 robot2 robot3 robot4 robot5 robot6 robot7 robot8 robot9- robot
   rgripper1 lgripper1 rgripper2 lgripper2 rgripper3 lgripper3 lgripper4 rgripper4 lgripper5 rgripper5 lgripper6 rgripper6 lgripper7 rgripper7 lgripper8 rgripper8 lgripper9 rgripper9- gripper
   room1 room2 room3 room4 room5 room6 room7 room8 room9- room
   ball1 ball2 ball3 ball4 ball5 ball6 ball7 ball8 ball9- ball)
(:init
(at-robby robot1 room1)
(free robot1 rgripper1)
(free robot1 lgripper1)
(at-robby robot2 room1)
(free robot2 rgripper2)
(free robot2 lgripper2)
(at-robby robot3 room3)
(free robot3 rgripper3)
(free robot3 lgripper3)
(at ball1 room2)
(at ball2 room2)
(at ball3 room1)
)
(:goal
(and
(at ball1 room1)
(at ball2 room3)
(at ball3 room3)
)
)
)


